﻿using UnityEngine;
using System.Collections;

public class Person {

	public float height;
	public float weight;
	public Transform personTransform;
	public float speed;

	public void Run()
	{
		personTransform.position += personTransform.forward * speed * Time.deltaTime;
	}

	public int Sum(int a, int b)
	{
		int sum = a + b;
		return sum;
	}

	public void Eat()
	{
	}
}



 public class Calculator
{
	public float Sum(float a, float b)
	{
		float c = a+b;
		return c;
	}

	public float Subtract(float a, float b)
	{
		float c = a-b;
		return c;
	}
}