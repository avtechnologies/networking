﻿using UnityEngine;
using System.Collections;
using MonoExtensions;

public class PhysicsBomb : MonoBehaviour {

	public GameObject explosionPrefab;
	public float impactRadius =20f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnCollisionEnter2D()
	{

		Vector3 explosionPos = transform.position;
		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 200f);
		Instantiate(explosionPrefab,transform.position,Quaternion.identity);
//		Debug.Break();
		foreach (Collider2D hit in colliders) 
		{
			Rigidbody2D rb = hit.GetComponent<Rigidbody2D>();
			if (rb != null)
			{
//				rb.AddForceAtPosition( (rb.transform.position-transform.position).normalized *10f, transform.position, ForceMode2D.Impulse);
				rb.AddExplosionForce(2200f,transform.position,impactRadius,15f);
				rb.SendMessageUpwards("EngineFail",2f,SendMessageOptions.DontRequireReceiver);
			}
		}

		Destroy (gameObject);
	}

	void OnDrawGizmos()
	{
		Vector3 position = transform.position;
		Vector3 previousPt =  new Vector3(position.x + impactRadius * Mathf.Cos (0),position.y + impactRadius * Mathf.Sin (0),position.z);
		float step = 360f/24f;
		for(float i=step;i<=360f;i+= step )
		{
			Vector3 nextPt = new Vector3(position.x + impactRadius * Mathf.Cos (Mathf.Deg2Rad *i),position.y + impactRadius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
			Gizmos.color = Color.red;
			Gizmos.DrawLine(previousPt,nextPt);
			previousPt = nextPt;
		}
	}
}
