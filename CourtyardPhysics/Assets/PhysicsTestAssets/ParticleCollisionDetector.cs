﻿using UnityEngine;
using System.Collections;

public class ParticleCollisionDetector : MonoBehaviour {

	public ParticleSystem laser;
	private ParticleCollisionEvent[] collisionEvents;
	void Awake()
	{
		collisionEvents = new ParticleCollisionEvent[10];
		laser = GetComponent<ParticleSystem>();
	}

	void OnParticleCollision(GameObject go)
	{
			laser.GetCollisionEvents (go, collisionEvents);
			if(collisionEvents.Length>0)
			{
				int index =0;
				ParticleCollisionEvent e = collisionEvents[index];
				while(e.intersection==Vector3.zero && index<collisionEvents.Length)
				{
					index++;
					e = collisionEvents[index];
				}
				go.transform.parent.GetComponent<Rigidbody2D>().AddForceAtPosition(-e.normal * 150f,e.intersection);
				go.SendMessageUpwards("ThrusterFail",SendMessageOptions.DontRequireReceiver);
			}
	}
}
