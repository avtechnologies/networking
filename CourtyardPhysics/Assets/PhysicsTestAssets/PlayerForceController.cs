﻿using UnityEngine;
using System.Collections;
using MonoExtensions;

public class PlayerForceController : Photon.MonoBehaviour {

	[HideInInspector]
	public Rigidbody2D rigidbody;
	public float TurnSpeed = 500;

	
	public PID angleControllerPID;
	public PID angularVelocityControllerPID;
	public PID brakePID;
	public PID speedPID;

	private float torque;
	public float targetAngle;
	private PhotonTransformView pTransformView;

	
	public float turnOvershootCorrectionSpeed = 10f;
	private bool isEngineEnabled = true;

	public float slowingDistance = 100f;
	public float stoppingDistance = 30f;

	[HideInInspector]
	public float engineForceMultiplier =0;
	private float engineRestartTime =0f;
	[HideInInspector]
	public float thrusterForceMultiplier =0f;
	public float brakeEfficiency = 0.5f;

	public Vector2 mouseDirection {
		get {
			return (input.mouseWorldPos - (Vector2)transform.position).normalized;
		}
	}

	public float maxSpeed = 30f;

	[HideInInspector]
	public Vector2 velocity {
		get {
			if(PhotonNetwork.isMasterClient)
			{
				return rigidbody.velocity;
			}
			else
			{
				return sampledVelocity;
			}
		}
	}

	private float angularVelocity {
		get {
			return rigidbody.angularVelocity;
		}
	}
	
	private PlayerInputNetworkController input;
	private Vector3 sampledVelocity;
	private Vector3 currentPosition;
	private Vector3 previousPosition;

	void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	void Start()
	{
		currentPosition = transform.position;
		previousPosition = currentPosition;
		input = PhotonView.Find((int)photonView.instantiationData[0]).GetComponent<PlayerInputNetworkController>();
		if(input.photonView.isMine)
		{
			//Hook up Camera
		}
	}
	
	void FixedUpdate()
	{
		currentPosition = transform.position;
		sampledVelocity = (currentPosition - previousPosition)/Time.fixedDeltaTime;
		previousPosition = currentPosition;


		if(PhotonNetwork.isMasterClient)
		{
			UpdatePhysics ();
		}

		if (isEngineEnabled) {
			engineForceMultiplier = Mathf.Lerp(engineForceMultiplier,1f,0.8f* Time.fixedDeltaTime);
		}
		thrusterForceMultiplier = Mathf.Lerp(thrusterForceMultiplier,1f,0.5f* Time.fixedDeltaTime);
		if(Time.time>engineRestartTime)
		{
			isEngineEnabled = true;
		}
	}

	#region System Failing Methods
		public void EngineFail (float seconds)
		{
			photonView.RPC ("EngineFailRPC", PhotonTargets.AllViaServer, seconds);
		}

		[PunRPC]
		public void EngineFailRPC (float seconds)
		{
			engineForceMultiplier = 0;
			isEngineEnabled = false;
			engineRestartTime = Time.time + seconds;
		}

		public void ThrusterFail ()
		{
			photonView.RPC ("ThrusterFailRPC", PhotonTargets.AllViaServer);
		}

		[PunRPC]
		public void ThrusterFailRPC (float seconds)
		{
			thrusterForceMultiplier = 0;
		}
	#endregion


 
	private float AngleDir(Vector3 fwd , Vector3 targetDir ,Vector3 up ) {
		float result=0;
		Vector3 perp  = Vector3.Cross(fwd, targetDir);
		float dir  = Vector3.Dot(perp, up);
		
		if (dir > 0.0) {
			result = 1.0f;
		} else if (dir < 0.0) {
			result = -1.0f;
		}

		return result * (Vector3.Angle(fwd, targetDir));
	}

	private void UpdatePhysics ()
	{
		Vector3 velocityDiff = (velocity.normalized - (Vector2)transform.up);
		if (isEngineEnabled) {
			float angleError = AngleDir (transform.up, mouseDirection, Vector3.forward);
			float torqueCorrectionForAngle = angleControllerPID.GetOutput (angleError, Time.fixedDeltaTime);
			float angularVelocityError = -angularVelocity;
			float torqueCorrectionForAngularVelocity = angularVelocityControllerPID.GetOutput (angularVelocityError, Time.fixedDeltaTime);
			torque = (torqueCorrectionForAngle + torqueCorrectionForAngularVelocity);
			DrawCircle (input.mouseWorldPos, slowingDistance, Color.blue);
			if (Vector2.Distance (transform.position, input.mouseWorldPos) > stoppingDistance) {
				rigidbody.AddTorque (torque * engineForceMultiplier * thrusterForceMultiplier);
			}
//			float distanceFromMouse = Vector2.Distance (transform.position, input.mouseWorldPos);
//			float k = Mathf.Min (distanceFromMouse, 15f) / distanceFromMouse;
//			if (Vector2.Distance (transform.position, input.mouseWorldPos) > slowingDistance) {
//				float speedError = maxSpeed - velocity.magnitude;
//				float speedCorrection = speedPID.GetOutput (speedError, Time.fixedDeltaTime);
//				rigidbody.AddForce (-velocityDiff * turnOvershootCorrectionSpeed * thrusterForceMultiplier, ForceMode2D.Force);
//				rigidbody.AddForce (transform.up * speedCorrection * k * engineForceMultiplier, ForceMode2D.Force);
//			}
//			else if (distanceFromMouse > stoppingDistance) 
//			{
//				float brakeError = velocity.magnitude;
//				float brakeCorrection = brakePID.GetOutput (brakeError, Time.fixedDeltaTime);
//				rigidbody.AddForce (-velocity.normalized * brakeCorrection * brakeEfficiency, ForceMode2D.Force);
//			}
		}
	}

	public static void DrawCircle(Vector3 position, float radius, Color color, int points = 24)
	{
		Vector3 previousPt =  new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad * 0),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * 0),position.z);
		float step = 360f/points;
		for(float i=step;i<=360f;i+= step )
		{
			Vector3 nextPt = new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad *i),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
			Debug.DrawLine(previousPt,nextPt,color);
			previousPt = nextPt;
		}
	}

	private void OnCollisionEnter2D(Collision2D  collisions)
	{
	
		if(collisions.relativeVelocity.magnitude>70f && collisions.transform!=null)
		{
			EngineFail(0.5f);
		}
	}
}