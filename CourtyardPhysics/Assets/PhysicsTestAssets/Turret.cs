﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {

	public Transform target;
	public float attackRadius =10f;
	public ParticleSystem fireSystem;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float particleSpeed = fireSystem.startSpeed;
		float distanceFromPlayer = Vector3.Distance(fireSystem.transform.position,target.position);
		float timeToReachPlayer = distanceFromPlayer/particleSpeed;

		Vector3 futurePosition = (Vector2)target.transform.position + (target.GetComponent<Rigidbody2D>().velocity * timeToReachPlayer);


		Vector3 dir= futurePosition - transform.position;
		Quaternion rot = Quaternion.FromToRotation(transform.up,dir);
		transform.rotation = Quaternion.Lerp(transform.rotation,transform.rotation*rot,10f * Time.deltaTime);

		if(Vector3.Distance(transform.position,target.position)<attackRadius)
		{
			if (!fireSystem.isPlaying) {
					fireSystem.Play();
			}
		}
		else{
			fireSystem.Stop();
		}
	}

	void OnDrawGizmos()
	{
		Vector3 position = transform.position;
		Vector3 previousPt =  new Vector3(position.x + attackRadius * Mathf.Cos (0),position.y + attackRadius * Mathf.Sin (0),position.z);
		float step = 360f/24f;
		for(float i=step;i<=360f;i+= step )
		{
			Vector3 nextPt = new Vector3(position.x + attackRadius * Mathf.Cos (Mathf.Deg2Rad *i),position.y + attackRadius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
			Gizmos.color = Color.red;
			Gizmos.DrawLine(previousPt,nextPt);
			previousPt = nextPt;
		}
	}
}
