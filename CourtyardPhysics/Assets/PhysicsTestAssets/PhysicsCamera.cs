﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonoExtensions;

public class PhysicsCamera : MonoBehaviour
{
	public PlayerInputNetworkController playerInput;
	public PlayerForceController player;
	public AnimationCurve playerTrackingCurve;
	public float velocityMagnitude = 0;
	public float normalMotionViewRadius = 30f;
	public float fastMotionViewRadius = 30f;
	private float deltaFocus =0;
	private float lerpedDeltaFocus =1f;
	private Vector3 velocity = Vector3.zero;
	private float fastSmoothTime = 0.030f;
	private float slowSmoothTime = 0.3f;
	public float smoothTime;
	public Vector3 focusVelocity;
	
	Vector3 mainFocusPoint;
	Vector3 externalFocusPoint;
	Vector3 netFocusPoint;
	public AnimationCurve entityFocusAttractionCurve;

	private static PhysicsCamera instance;
	public static PhysicsCamera Instance
	{
		get{
			if(instance==null)
			{
				instance = FindObjectOfType<PhysicsCamera>();
			}
			return instance;
		}
	}

	private void Awake()
	{
		instance = this;
		enabled = false;
	}

	

	// Update is called once per frame
	private void FixedUpdate ()
	{
		float dt = Time.fixedDeltaTime;
		velocityMagnitude = player.velocity.magnitude;
		Vector3 camPosOnPlayerPlane = transform.position.ReplaceComponent (VectorComponent.Z, playerInput.transform.position.z);
		float playerTrackPercentage = playerTrackingCurve.Evaluate (playerInput.viewPortDistanceFromCenter);

		ScriptExtensions.DrawCircle (camPosOnPlayerPlane, normalMotionViewRadius, Color.blue);
		ScriptExtensions.DrawCircle (camPosOnPlayerPlane, fastMotionViewRadius, Color.green);
		mainFocusPoint =  playerInput.transform.position + (playerInput.transform.up * (normalMotionViewRadius * playerTrackPercentage));
		netFocusPoint = mainFocusPoint;

		deltaFocus = Vector2.Dot(playerInput.transform.up,player.mouseDirection)/2f +0.5f; 
		lerpedDeltaFocus = Mathf.Min(lerpedDeltaFocus,deltaFocus);
		lerpedDeltaFocus = Mathf.Lerp(lerpedDeltaFocus,deltaFocus,0.8f*dt);
		smoothTime = Mathf.Lerp(slowSmoothTime,fastSmoothTime,lerpedDeltaFocus);
		ScriptExtensions.DrawCross (netFocusPoint, 2f, Color.black);

		if(player.engineForceMultiplier * player.thrusterForceMultiplier==0)
		{
			float d = Vector3.Distance (playerInput.transform.position, camPosOnPlayerPlane);
			if (d > fastMotionViewRadius) 
			{
				Vector3 dir = (playerInput.transform.position - camPosOnPlayerPlane).normalized;
				float diff = d - fastMotionViewRadius;
				Vector3 newTarget = camPosOnPlayerPlane + (dir * diff);
				ScriptExtensions.DrawCross (newTarget, 2f, Color.yellow);
				transform.position = Vector3.SmoothDamp(transform.position, newTarget.ReplaceComponent(VectorComponent.Z,transform.position.z), ref velocity, slowSmoothTime);
			}
		}
		else
		{
			float netSmoothTime = smoothTime + (1f-player.engineForceMultiplier)  + (1f-player.thrusterForceMultiplier);
			transform.position = Vector3.SmoothDamp(transform.position, netFocusPoint.ReplaceComponent(VectorComponent.Z,transform.position.z), ref velocity, netSmoothTime );
		}
	}

	private CameraFocusAttractor GetFocusAttractor(Vector3 mainAttractorPosition, float minimumDistance)
	{
		CameraFocusAttractor[] attractors = FindObjectsOfType<CameraFocusAttractor>();
		List<CameraFocusAttractor> candidates = new List<CameraFocusAttractor>();
		foreach (var cameraFocusAttractor in attractors) {
			float distance = Vector3.Distance(mainAttractorPosition,cameraFocusAttractor.transform.position);
			if(distance<minimumDistance)
			{
				cameraFocusAttractor.distance = distance;
				candidates.Add(cameraFocusAttractor);
			}
		}
		if(candidates.Count>0)
		{
			candidates.Sort((a,b)=>{ return a.distance.CompareTo(b.distance);});
			return candidates[0];
		}
		return null;
	}
}
