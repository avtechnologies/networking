﻿using UnityEngine;
using System.Collections;

public class NebulaAnimator : MonoBehaviour {
	public SpriteRenderer nebula1;
	public SpriteRenderer nebula2;

	public float perlin1Offset =1f;
	public float perlin2Offset =1f;

	
	// Update is called once per frame
	void Update () {
		float rot1 = Mathf.PerlinNoise (Mathf.Sin(Time.time), Time.time) * perlin1Offset;
		nebula1.transform.Rotate (0,0,rot1 * Time.deltaTime);

		float rot2 = Mathf.PerlinNoise (Time.time, Time.time) * perlin2Offset;
		nebula2.transform.Rotate (0,0,-rot2 * Time.deltaTime);

	}
}
