﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityExtensions
{
	public static class Extensions
	{
		public static T[] GetInterfacesInChildren<T>(this GameObject str) where T: Object
		{
			Object[]  monoBehaviours = str.GetComponentsInChildren<Object>();
			List<T> interfaceList = new List<T>();
			foreach(Object m in monoBehaviours)
			{
				T i = (T)m;
				if(i!=null)
				{
					interfaceList.Add(i);
				}
			}
			return interfaceList.ToArray();
		}

		public static T GetInterface<T>(this GameObject str) where T: Object
		{
			Object  monoBehaviour = str.GetComponent<Object>();
			return (T)monoBehaviour;
		}
	}   
}
