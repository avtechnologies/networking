﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundGenerator : MonoBehaviour {

	public float viewportLimit= 0.1f;
	public SpriteRenderer srPrefab;
	public float width;
	public float height;

	private Dictionary<Vector3,SpriteRenderer> spriteDictionary = new Dictionary<Vector3, SpriteRenderer> ();
	List<Vector3> removalList = new List<Vector3>();

	// Use this for initialization
	void Start () {

		Bounds b = srPrefab.bounds;
		width = b.max.x - b.min.x;
		height = b.max.z - b.min.z;
	}

	
	// Update is called once per frame
	void Update () {
		float cameraYdistance = Camera.main.transform.position.y - transform.position.y;

		Vector3 cameraCenter = Camera.main.ViewportToWorldPoint (new Vector3(0.5f,0.5f,cameraYdistance));
		
		Vector3[] viewportCorners = new Vector3[4]; 
		viewportCorners[0] = Camera.main.ViewportToWorldPoint ( new Vector3 (-viewportLimit, -viewportLimit, cameraYdistance));
		viewportCorners[1] = Camera.main.ViewportToWorldPoint (new Vector3(1f+ viewportLimit,-viewportLimit,cameraYdistance));
		viewportCorners[2] = Camera.main.ViewportToWorldPoint ( new Vector3 (-viewportLimit, 1f+ viewportLimit, cameraYdistance));
		viewportCorners[3] = Camera.main.ViewportToWorldPoint (new Vector3(1f+ viewportLimit,1f+ viewportLimit,cameraYdistance));

		float cWidth = viewportCorners[3].x - viewportCorners[0].x;
		float cHeight = viewportCorners[3].z - viewportCorners[0].z;

		Bounds cameraWordBounds = new Bounds(cameraCenter,new Vector3(cWidth,0,cHeight));
		
		foreach (Vector3 v in viewportCorners) {
			Vector3 imgPosition = GetGridBoxPosition(v);
			if(!spriteDictionary.ContainsKey(imgPosition))
			{
				spriteDictionary.Add(imgPosition,CreateImage (imgPosition));
			}
		}

		foreach (KeyValuePair<Vector3,SpriteRenderer> k in spriteDictionary) {
			if(!BoxesIntersect(GetImageBounds(k.Key),cameraWordBounds))
			{
				removalList.Add(k.Key);
				Destroy(k.Value.gameObject);
			}
		}

		foreach (Vector3 v in removalList) {
			spriteDictionary.Remove(v);
		}
		removalList.Clear ();
	}

	private SpriteRenderer CreateImage(Vector3 pos)
	{
		SpriteRenderer r = Instantiate<SpriteRenderer>(srPrefab);
		r.transform.parent = transform;
		r.transform.position = pos;
		return r;
	}

	private Bounds GetImageBounds(Vector3 center)
	{
		return new Bounds(center,new Vector3(width,0,height));
	}

	Vector3 GetGridBoxPosition(Vector3 pos)
	{
		float gridX = Mathf.Floor (pos.x / width);
		float gridZ = Mathf.Floor (pos.z / height);

		return new Vector3 (gridX * width, transform.position.y, gridZ * height) + new Vector3 (width/2f,transform.position.y,height/2f);
	}

	bool BoxesIntersect(Bounds a, Bounds b)
	{
		if (a.max.x < b.min.x){ return false; }// a is left of b
		if (a.min.x > b.max.x){ return false; }// a is right of b
		if (a.max.z < b.min.z){ return false; } // a is above b
		if (a.min.z > b.max.z){ return false; }// a is below b
		return true; // boxes overlap
	}
}
