﻿using UnityEngine;
using System.Collections;

public class SpawnPosition : MonoBehaviour {

	private Vector3 position;
	public float AREA_CHECK_RADIUS=200f;
	public LayerMask mask;
	// Use this for initialization
	void Start () {
		position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//Lock the position so that it cannot be changed
		transform.position = position;
	}

	public bool CheckSpace()
	{
		return Physics.CheckSphere(transform.position,AREA_CHECK_RADIUS*0.9f,mask);
	}


	void OnDrawGizmos()
	{
		Gizmos.color = new Color(0,1f,0,0.3f);
		Gizmos.DrawSphere(transform.position, AREA_CHECK_RADIUS);

		transform.position = new Vector3(transform.position.x,0,transform.position.z);
	}
}
