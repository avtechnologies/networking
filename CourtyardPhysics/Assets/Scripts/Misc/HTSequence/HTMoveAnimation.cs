using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Core;
using System;

public enum AnimationSpace
{
		World,
		Local
}

public class HTMoveAnimation : HTControl
{
		public Vector3 positionOffset;
		public AnimationSpace space;
		[HideInInspector]
		public Vector3
				gizmosCenter;

		void OnDrawGizmosSelected ()
		{
				MeshFilter mf = GetComponent<MeshFilter> ();
				RectTransform rTransform = GetComponent<RectTransform>();
				if (mf != null) {
						Gizmos.color = new Color (0f, 1f, 0f, 0.15f);
						gizmosCenter = transform.TransformPoint (mf.sharedMesh.bounds.center) + positionOffset;
						Gizmos.DrawCube (gizmosCenter, Matrix4x4.TRS (transform.position, transform.rotation, transform.localScale) * mf.sharedMesh.bounds.size);
				} else if(mf!=null){
						BoxCollider b = GetComponent<BoxCollider> ();
						if (b != null) {
								Gizmos.color = new Color (0f, 1f, 0f, 0.15f);
								Gizmos.DrawCube (transform.position + positionOffset, b.size);
						}
				} else if(rTransform!=null)
				{
					
					RectTransform rectTransform = GetComponent<RectTransform>();
					Vector3[] corners = new Vector3[4];
					rectTransform.GetWorldCorners(corners);
//					for (int i = 0; i < corners.Length; i++) {
//						Vector3 v = corners [i];
//						if(i==0)
//						{
//							Gizmos.color = Color.red;
//						}else if(i==2)
//						{
//							Gizmos.color = Color.blue;
//						}
//						else
//						{
//							Gizmos.color = Color.white;
//						}
//						Gizmos.DrawSphere (v, 30f);
//					}
					
					Gizmos.color = new Color(0,1f,0,0.3f);
					
					
					Vector3 centerPt =  ((corners[2] - corners[0])/2f) + corners[0];
					Gizmos.DrawCube(centerPt + positionOffset,new Vector3(corners[2].x-corners[0].x,corners[2].y-corners[0].y,0  ));
					Gizmos.DrawSphere(centerPt,30f);
				}
		}

	#region implemented abstract members of HTControl
		public override Holoville.HOTween.Tweener ToAnimation ()
		{
				string attr = (space == AnimationSpace.Local) ? "localPosition" : "position";
		
				if (easeType == EaseType.AnimationCurve) {
						tween = HOTween.To (transform, duration, new TweenParms ().Prop (attr, positionOffset, true).Delay (delay).Ease (curve));
				} else {
						tween = HOTween.To (transform, duration, new TweenParms ().Prop (attr, positionOffset, true).Delay (delay).Ease (easeType));
				}
				return tween;
		}

		public override Holoville.HOTween.Tweener FromAnimation ()
		{
				string attr = (space == AnimationSpace.Local) ? "localPosition" : "position";
		
				if (easeType == EaseType.AnimationCurve) {
						tween = HOTween.From (transform, duration, new TweenParms ().Prop (attr, positionOffset, true).Delay (delay).Ease (curve));
				} else {
						tween = HOTween.From (transform, duration, new TweenParms ().Prop (attr, positionOffset, true).Delay (delay).Ease (easeType));
			 
				}	
				return tween;
		}
	#endregion

}
