﻿using UnityEngine;
using System.Collections;

public interface IDamageable {

	[PunRPC]
	void ApplyDamage(float damage, PhotonPlayer attacker);
	PhotonView pView {get;}
}
