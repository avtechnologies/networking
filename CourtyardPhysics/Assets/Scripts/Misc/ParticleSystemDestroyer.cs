﻿using UnityEngine;
using System.Collections;

public class ParticleSystemDestroyer : MonoBehaviour {

	private ParticleSystem ps;
	// Use this for initialization
	void Awake () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!ps.isPlaying)
		{
			Destroy(gameObject);
		}
	}
}
