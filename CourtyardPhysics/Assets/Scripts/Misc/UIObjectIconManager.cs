using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIObjectIconManager : MonoBehaviour {

	public static UIObjectIconManager instance;
	private Dictionary<UIIconId,List<UIObjectIcon>> iconDictionary = new Dictionary<UIIconId, List<UIObjectIcon>>();
	void Awake()
	{
		instance = this;
		CreateIconPool ();
		print ("UIObjectIconManager instance set");
	}


	private void CreateIconPool()
	{
		UIObjectIcon[] objectIcons = GetComponentsInChildren<UIObjectIcon> ();
		foreach (UIObjectIcon i in objectIcons) {
			List<UIObjectIcon> iconList = new List<UIObjectIcon>();
			iconList.Add(i);
			i.enabled = false;
			i.ToggleVisibility(false);
			for(int p =0 ; p<i.poolSize;p++)
			{
				UIObjectIcon newIcon = UIObjectIcon.Instantiate(i) as UIObjectIcon;
				newIcon.GetComponent<RectTransform>().SetParent(i.transform.parent);
				iconList.Add(newIcon);
				newIcon.transform.localScale = i.transform.localScale;
			}
			iconDictionary.Add(i.iconId,iconList);
		}
	}

	public T SetIcon<T>(UIIconId id,GameObject worldObject) where T : UIObjectIcon
	{
		List<UIObjectIcon> iconList = iconDictionary [id];
		foreach (UIObjectIcon icon in iconList) {
			if(!icon.enabled)
			{
				icon.enabled = true;
				icon.ToggleVisibility(true);
				icon.WorldObject = worldObject;
				return (T)icon;
			}
		}
		return null;
	}

	public void UnsetIcon(UIObjectIcon icon)
	{
		if(icon!=null)
		{
			icon.enabled = false;
			icon.ToggleVisibility (false);
		}
	}

}
