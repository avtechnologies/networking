﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	private AudioSource aSource;

	void Awake()
	{
		aSource = GetComponent<AudioSource> ();
		transform.parent = null;
	}

	void Update()
	{
		if (!aSource.isPlaying) {
			Destroy(gameObject);
		}
	}
}
