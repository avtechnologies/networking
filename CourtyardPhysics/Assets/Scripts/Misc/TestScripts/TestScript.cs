﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectUsingSettings("v1.0");

	}
	
	public int ping;
	// Update is called once per frame
	void Update () {
		ping = PhotonNetwork.GetPing();
	
	}
	
	
	void OnConnectedToPhoton()
	{
		Debug.Log("Connected to Photon");
	}
	
	void OnJoinedLobby()
	{
		RoomOptions ro = new RoomOptions();
		ro.isVisible = true;
		ro.isOpen = true;
		TypedLobby tl = new TypedLobby();
		PhotonNetwork.JoinOrCreateRoom("courtyard",ro,tl);
	}
	
	void OnJoinedRoom()
	{
		Debug.Log ("Joined Room  , Player count: " + 		PhotonNetwork.playerList.Length );
		PhotonNetwork.Instantiate("Orb",new Vector3(Random.Range(-100f,100f),0f,Random.Range(-100f,100f)),Quaternion.identity,0);
	}
}
