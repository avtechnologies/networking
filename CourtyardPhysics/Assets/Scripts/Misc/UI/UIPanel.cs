﻿using UnityEngine;
using System.Collections;
using System;

public class UIPanel : MonoBehaviour {

	public HTSequenceId panelId;
	protected UIController uiController;
	public object data;
	public Action OnOpenCompleteAction;

	protected virtual void Awake()
	{
		uiController = transform.root.GetComponentInChildren<UIController>();
	}

	
	public virtual void Open()
	{
		gameObject.SetActive(true);
		//Debug.Log("Playing " + name);
		HTSequence.Instance.Play(panelId,false,1f,()=>{
				OnOpenComplete();
				if(OnOpenCompleteAction!=null)
				{
					OnOpenCompleteAction();
				}
		});
	}
	
	protected virtual void OnOpenComplete()
	{
	}
	
	public virtual void Close()
	{
		HTSequence.Instance.Play(panelId,true,1f,()=>{
		OnCloseComplete();
		gameObject.SetActive(false);
		});
	}
	
	protected virtual void OnCloseComplete()
	{
	}
	
	public void Dim(GameObject obj, float alpha)
	{
//		tk2dBaseSprite[] sprites = obj.GetComponentsInChildren<tk2dBaseSprite>();
//		tk2dTextMesh[] textMeshes = obj.GetComponentsInChildren<tk2dTextMesh>();
//		
//		foreach (var tk2dBaseSprite in sprites) {
//			tk2dBaseSprite.color = new Color(tk2dBaseSprite.color.r,tk2dBaseSprite.color.g,tk2dBaseSprite.color.b,alpha);
//		}
//		
//		foreach (var tk2dTextMesh in textMeshes) {
//			tk2dTextMesh.color = new Color(tk2dTextMesh.color.r,tk2dTextMesh.color.g,tk2dTextMesh.color.b,alpha);
//		}
	}
	
	protected void ClearMemory()
	{
		UIController.Instance.StartCoroutine(ClearMemoryCoroutine());
	}
	
	protected IEnumerator ClearMemoryCoroutine()
	{
		System.GC.Collect();
		yield return Resources.UnloadUnusedAssets();
	}

}
