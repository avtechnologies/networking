﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[CustomEditor(typeof(Network2DPhysicsConfiguration))]
public class Network2DPhysicsEditor : Editor {
	
	public override void OnInspectorGUI ()
	{
		Network2DPhysicsConfiguration v = (Network2DPhysicsConfiguration)target;
		if(!v.GetComponent<PhotonView>())
		{
			Rigidbody2D rigidbody = v.gameObject.GetComponent<Rigidbody2D>();
			if(!rigidbody)
			{
				rigidbody = v.gameObject.AddComponent<Rigidbody2D>();
			}
			rigidbody.sleepMode = RigidbodySleepMode2D.StartAsleep;

			PhotonTransformView tView = v.gameObject.AddComponent<PhotonTransformView>();
			SerializedObject serializedObject = new SerializedObject(tView);
			Debug.Log("seriualizedObject " + serializedObject.GetType().ToString());
			SerializedProperty m_SynchronizePositionProperty = serializedObject.FindProperty("m_PositionModel.SynchronizeEnabled");
			SerializedProperty m_SynchronizeRotationProperty = serializedObject.FindProperty("m_RotationModel.SynchronizeEnabled");
			SerializedProperty m_SynchronizeScaleProperty = serializedObject.FindProperty("m_ScaleModel.SynchronizeEnabled");
			
			m_SynchronizePositionProperty.boolValue = true;
			m_SynchronizeRotationProperty.boolValue = true;
			m_SynchronizeScaleProperty.boolValue = false;


			SerializedProperty interpolateProperty = serializedObject.FindProperty("m_PositionModel.InterpolateOption");
			interpolateProperty.enumValueIndex = (int)PhotonTransformViewPositionModel.InterpolateOptions.SynchronizeValues;
			SerializedProperty extrapolateProperty = serializedObject.FindProperty("m_PositionModel.ExtrapolateOption");
			extrapolateProperty.enumValueIndex = (int)PhotonTransformViewPositionModel.ExtrapolateOptions.SynchronizeValues;
   			SerializedProperty teleportEnabled = serializedObject.FindProperty("m_PositionModel.TeleportEnabled");
			teleportEnabled.boolValue = false;

			interpolateProperty = serializedObject.FindProperty("m_RotationModel.InterpolateOption");
			interpolateProperty.enumValueIndex = (int)PhotonTransformViewRotationModel.InterpolateOptions.Lerp;
			
			serializedObject.ApplyModifiedProperties();

			PhotonView pv = v.gameObject.AddComponent<PhotonView>();
			pv.ObservedComponents = new System.Collections.Generic.List<Component>();
			pv.ObservedComponents.Add(tView);
			pv.synchronization = ViewSynchronization.Unreliable;

			EditorUtility.SetDirty(tView);
			EditorUtility.SetDirty(pv);
			AssetDatabase.SaveAssets();
		}
	}
}
