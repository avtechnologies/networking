﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Network2DPhysicsConfiguration))]
public class Network2DPhysicsObject : Photon.MonoBehaviour {

	private PhotonTransformView transformView;
	private Rigidbody2D rigidbody;
	public float smootheningFactor = 30f;

	void Start()
	{
		transformView = GetComponent<PhotonTransformView>();
		rigidbody = GetComponent<Rigidbody2D>();
		if(!PhotonNetwork.isMasterClient)
		{
			rigidbody.isKinematic = true;
			Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
			foreach (var collider2D in colliders) {
				collider2D.enabled = false;
			}
		}
		else
		{
			rigidbody.WakeUp();
		}
	}

		
	private Vector3 lastVelocity;
	private float lastAngularVelocity;
	private Vector2 currentVelocity;
	private float currentAngulerVelocity;

	private Vector2 velocityCorrection;

	private void Update()
	{
		if(PhotonNetwork.isMasterClient)
		{
			currentVelocity = Vector2.SmoothDamp(currentVelocity,rigidbody.velocity,ref velocityCorrection,0.04f);
			currentAngulerVelocity = Mathf.Lerp(currentAngulerVelocity,rigidbody.angularVelocity,smootheningFactor * Time.deltaTime);
			
			transformView.SetSynchronizedValues(currentVelocity,currentAngulerVelocity);
			lastVelocity = rigidbody.velocity;
			lastAngularVelocity = rigidbody.angularVelocity;
		}
	}
}
