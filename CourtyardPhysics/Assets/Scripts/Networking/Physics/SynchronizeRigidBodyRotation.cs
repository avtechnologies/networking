﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
public class SynchronizeRigidBodyRotation : Photon.MonoBehaviour {

	private bool m_ReceivedNetworkUpdate = false;
	private double m_LastSerializeTime;
	private float m_SynchronizedTurnSpeed;
	private Vector2 m_NetworkUpDirection;
	private Rigidbody2D rigidbody;

	public void OnPhotonSerializeView( PhotonStream stream, PhotonMessageInfo info )
	{
		OnPhotonSerializeRotationView( (Vector2)transform.up, stream, info );
		
		if( stream.isReading == true )
		{
			m_ReceivedNetworkUpdate = true;
		}
	}

	public void OnPhotonSerializeRotationView( Vector2 upDirection, PhotonStream stream, PhotonMessageInfo info )
	{
		if( stream.isWriting == true )
		{
			stream.SendNext( upDirection );
			stream.SendNext( m_SynchronizedTurnSpeed );
		}
		else
		{
			m_NetworkUpDirection = (Vector2)stream.ReceiveNext();
			m_SynchronizedTurnSpeed = (float)stream.ReceiveNext();
//			Debug.Log(m_NetworkZRotation + "    " + m_SynchronizedTurnSpeed);
		}
		
		m_LastSerializeTime = PhotonNetwork.time;
	}


	public void SetSynchronizedValues(float turnSpeed )
	{
		m_SynchronizedTurnSpeed = turnSpeed;
	}

	void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}
	

	void Update()
	{
		if(m_ReceivedNetworkUpdate == false || photonView.isMine == true)
		{
			SetSynchronizedValues(rigidbody.angularVelocity);
			return;
		}
		transform.up = UpdateUpDirection( transform.up);
	}

	public Vector3 UpdateUpDirection( Vector2 currentUpVector)
	{
		Vector2 targetZUp = Quaternion.Euler(0,0, GetExtrapolatedRotationOffset()) * m_NetworkUpDirection;

		if( m_SynchronizedTurnSpeed == 0 )
		{
			return targetZUp;
		}
		else
		{
			return Vector3.RotateTowards(currentUpVector.normalized,targetZUp.normalized,Mathf.Deg2Rad * Mathf.Abs(m_SynchronizedTurnSpeed)* Time.deltaTime,1f);
		}
	}

	public float GetExtrapolatedRotationOffset()
	{
		float timePassed = (float)( PhotonNetwork.time - m_LastSerializeTime );
		timePassed += (float)PhotonNetwork.GetPing() / 1000f;
		return m_SynchronizedTurnSpeed * timePassed;
	}
}


