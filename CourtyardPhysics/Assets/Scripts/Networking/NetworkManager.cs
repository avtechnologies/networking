using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class NetworkManager : Photon.MonoBehaviour {

	public string roomName="Hell";

	public int ping;
	private static NetworkManager instance;

	public Action joinedLobbyAction;
	public Action joinedRoomAction;

	void Awake()
	{
		instance = this;
	}

	public static void Connect(string settings, Action joinedLobbyAction)
	{
		instance.joinedLobbyAction = joinedLobbyAction;
	}

	void OnJoinedLobby()
	{
		if(joinedLobbyAction!=null)
		{
			joinedLobbyAction();
			joinedLobbyAction = null;
		}
	}

	public static void JoinRoom(string nick, Action joinedRoomAction)
	{
		instance.joinedRoomAction = joinedRoomAction;
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.isVisible = true;
		roomOptions.isOpen = true;
		TypedLobby typedLobby = new TypedLobby();
		PhotonNetwork.player.name = nick;
		PhotonNetwork.JoinOrCreateRoom(instance.roomName,roomOptions,typedLobby);
	}

	void OnJoinedRoom()
	{
		if(joinedRoomAction!=null)
		{
			joinedRoomAction();
			joinedRoomAction = null;
		}

	}

	void OnMasterClientSwitched ( PhotonPlayer newMasterClient )
	{
		if (PhotonNetwork.isMasterClient) {
//			photonView.RPC("ReceiveSpawnPositions",PhotonTargets.OthersBuffered,GetSerializedNetworkSyncedObjects());
		}
	}

	private  void Update()
	{
		ping = PhotonNetwork.GetPing();
	}
}
