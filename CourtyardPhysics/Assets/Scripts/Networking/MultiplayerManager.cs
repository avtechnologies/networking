﻿using UnityEngine;
using System.Collections;

public enum NetEvents
{
	CreatePlayer =0
}

public class MultiplayerManager : Photon.MonoBehaviour {

	private static MultiplayerManager instance;
 	public static MultiplayerManager Instance
 	{
	 	get{
	 		if(!instance)
	 		{
	 			instance = FindObjectOfType<MultiplayerManager>();
	 		}
	 		return instance;
	 	}
	 }

 	private void Awake()
 	{
 		instance = this;
	}
	
	void OnJoinedRoom()
	{
		if(!PhotonNetwork.isMasterClient)
		{
			PhotonNetwork.Instantiate("PlayerNetworkController",Vector3.zero,Quaternion.identity,0);
		}
	}

	void HandleEventCall (byte eventCode, object content, int senderId)
	{
		NetEvents e = (NetEvents)eventCode;
		switch(e)
		{
			case NetEvents.CreatePlayer:
			break;
		}
	}

	public void CreatePlayerFighter(PlayerInputNetworkController playerInput)
	{
		Debug.Log("Creating fighter");
		PhotonNetwork.InstantiateSceneObject("PlayerFighter",Vector3.zero,Quaternion.identity,0,new object[]{playerInput.photonView.viewID});
	}
}
