﻿using UnityEngine;
using System.Collections;
using MonoExtensions;

public class PlayerInputNetworkController : Photon.MonoBehaviour {

	private PhotonTransformView transformView;
	private PlayerForceController player;
	public PhotonPlayer photonPlayer;

	private Vector3 mousePosition
	{
		get{
			return new Vector3(transform.position.x,transform.position.y,0);
		}
	}

	public float viewPortDistanceFromCenter
	{
		get{
			return Mathf.Min(Vector3.Distance(Camera.main.ScreenToViewportPoint(mousePosition),new Vector3(0.5f,0.5f))/0.5f,1f);
		}
	}

	public Vector2 mouseWorldPos
	{
		get{
			return transform.position;
		}
	}

	private void Awake()
	{
		transformView = GetComponent<PhotonTransformView>();
	}

	GameObject g;
	public bool isMine = false;
	void Start()
	{
		g = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		isMine = photonView.isMine;
		previousPosition = transform.position;
		if(PhotonNetwork.isMasterClient)
		{
			MultiplayerManager.Instance.CreatePlayerFighter(this);
		}
	}

	public Vector3 sampledVelocity;
	public float velocityLerpTime =0.5f;
	private Vector3 previousPosition;

	private void Update()
	{
		g.transform.position = transform.position;
		if(photonView.isMine)
		{
			transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition.ReplaceComponent(VectorComponent.Z,Mathf.Abs(Camera.main.transform.position.z)));
			sampledVelocity =  Vector3.Lerp(sampledVelocity, (transform.position - previousPosition)/Time.deltaTime, velocityLerpTime * Time.deltaTime);
			previousPosition = transform.position;
			transformView.SetSynchronizedValues(sampledVelocity,0f);
		}
	}
}
