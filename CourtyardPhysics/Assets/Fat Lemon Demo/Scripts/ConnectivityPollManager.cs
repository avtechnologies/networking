using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;

/// <summary>
/// This component is responsible for continously checking if there is an internet connection.
/// This is done by first checking the Application.internetReachability. But this can give false
/// positives, e.g. connected to a wifi router but does not have an actual internet connection.
/// So the next step is pinging PING_DESTINATION every TIME_BETWEEN_PINGS seconds.
/// If the ping fails for FAILED_PINGS_DISCONNECTION_TRIGGER times, it means it has gone offline.
/// Whenever the connectivity changes, a ConnectivityChange notification is fired.
/// </summary>
public class ConnectivityPollManager : MonoBehaviour
{
	static bool _hasInternet;
	public static bool HasInternet { get { return _hasInternet; } }
	
	const string PING_DESTINATION = "google.com";
	const float TIME_BETWEEN_PINGS = 3; //seconds
	//sometimes the ping just fails every once in a while, so we should do multiple pings just to make sure
	const int FAILED_PINGS_DISCONNECTION_TRIGGER = 3;

	string _address;
	int _failedPings;
        
	void Start ()
	{
		StartCoroutine (CheckForConnectivityCoroutine ());
	}
        
	IEnumerator CheckForConnectivityCoroutine ()
	{
		bool hasPingedDestination = false;

		while (true) {
			bool isNetworkReachable = (Application.internetReachability != NetworkReachability.NotReachable);
			//If the api says that it is reachable, we need to test further by pinging a proper connection
			if (isNetworkReachable) {

				//Inspired from http://forum.unity3d.com/threads/68938-How-can-you-tell-if-there-exists-a-network-connection-of-any-kind
				//Its result is stored in _hasPingedDestination, since coroutines cannot return a value directly
				//IEnumerator TestConnectionCoroutine()
				hasPingedDestination = false;
				WWW www = new WWW (PING_DESTINATION);
				yield return www;
				if (www.error == null) {
					hasPingedDestination = true;
				}

				if (hasPingedDestination != _hasInternet) {
					if (!hasPingedDestination) {
						++_failedPings;
						if (_failedPings >= FAILED_PINGS_DISCONNECTION_TRIGGER) {
							Debug.Log ("Went offline");
							_hasInternet = false;
							NotificationCenter.Post (NotificationType.ConnectivityChange, hasPingedDestination);
						} else {
							Debug.Log ("Ping missed");
						}
					} else {
						Debug.Log ("Back online");
						_hasInternet = true;
						_failedPings = 0;
						NotificationCenter.Post (NotificationType.ConnectivityChange, hasPingedDestination);
					}
				}
			} else {
				//Network was not reachable
				if (_hasInternet) {
					Debug.Log ("Went offline (airplane mode/wifi off?)");
					_hasInternet = false;
					NotificationCenter.Post (NotificationType.ConnectivityChange, hasPingedDestination);
				}
			}
			
			float timeToNextCheck = Time.realtimeSinceStartup + TIME_BETWEEN_PINGS;
			while (Time.realtimeSinceStartup < timeToNextCheck) {
				//Not using WaitForSeconds so that it still checks while the game is paused
				yield return null;
			}
		}
	}
}
