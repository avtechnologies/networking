﻿using UnityEngine;
using System.Collections;

public class FrameRateUtil : MonoBehaviour
{
	
	// Attach this to a GUIText to make a frames/second indicator.
	//
	// It calculates frames/second over each updateInterval,
	// so the display does not keep changing wildly.
	//
	// It is also fairly accurate at very low FPS counts (<10).
	// We do this not by simply counting frames per interval, but
	// by accumulating FPS for each frame. This way we end up with
	// correct overall FPS even if the interval renders something like
	// 5.5 frames.
	
	public  float updateInterval = 0.5f;
	public Vector2 pixelOffset;
	private float accum = 0;	// FPS accumulated over the interval
	private int frames = 0;		// Frames drawn over the interval
	private float timeleft;		// Left time for current interval
	private float fps;
	
	void Start()
	{
		timeleft = updateInterval;  
	}
	
	void Update()
	{
		timeleft -= Time.deltaTime;
		accum += Time.timeScale / Time.deltaTime;
		++frames;
		
		// Interval ended - update GUI text and start new interval
		if (timeleft <= 0f)
		{
			fps = accum / frames;

			timeleft = updateInterval;
			accum = 0f;
			frames = 0;
		}
	}

	void OnGUI()
	{
		Color originalColor = GUI.color;

		if (fps < 30)
		{
			GUI.color = Color.yellow;
		} else 
			if (fps < 10)
		{
			GUI.color = Color.red;
		} else
		{
			GUI.color = Color.green;
		}

		string lblFPS = string.Format("{0:F2} FPS", fps);
		Vector2 size = GUI.skin.GetStyle("label").CalcSize(new GUIContent(lblFPS));
		GUI.Label(new Rect(Screen.width - pixelOffset.x - size.x, pixelOffset.y, size.x, size.y), lblFPS);
		GUI.color = originalColor;
	}
}