using UnityEngine;
using System.Collections;

public class MathUtils
{
	public static float Map (float val, float inputMin, float inputMax, float outputMin, float outputMax)
	{
		AVDebug.Assert (inputMax > inputMin, "The variable inputMax must be greater than inputMin");
		AVDebug.Assert (outputMax > outputMin, "The variable outputMax must be greater than outputMin");
		return outputMax - (((inputMax - val) / (inputMax - inputMin)) * (outputMax - outputMin));
	}
}
