﻿using UnityEngine;
using System.Collections;

public class ColorUtils
{
	public static string ColorToHex (Color color)
	{
		return ((int)(color.r * 255)).ToString ("X2") + ((int)(color.g * 255)).ToString ("X2") + ((int)(color.b * 255)).ToString("X2") + ((int)(color.a * 255)).ToString ("X2");
	}
	
	public static Color HexToColor (string color)
	{
		color = color.PadRight (8, '0');
		float r = int.Parse (color.Substring (0, 2), System.Globalization.NumberStyles.HexNumber) / 255f;
		float g = int.Parse (color.Substring (2, 2), System.Globalization.NumberStyles.HexNumber) / 255f;
		float b = int.Parse (color.Substring (4, 2), System.Globalization.NumberStyles.HexNumber) / 255f;
		float a = int.Parse (color.Substring (6, 2), System.Globalization.NumberStyles.HexNumber) / 255f;
		return new Color (r, g, b, a);
	}
}
