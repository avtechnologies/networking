
/// <summary>
/// This is a wrapper for a notification
/// The extra information associated with a notification can be placed in the data
/// </summary>
public class Notification
{
	public const bool KEEP_ON_RESET = true;
	
	public NotificationType type;
	public object data;

	public Notification(NotificationType type)
	{
		this.type = type;
	}

	public Notification(NotificationType type, object userInfo)
	{
		this.type = type;
		this.data = userInfo;
	}
}
