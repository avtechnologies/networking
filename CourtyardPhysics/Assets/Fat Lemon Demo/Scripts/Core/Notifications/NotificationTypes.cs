using UnityEngine;
using System.Collections;

//ALWAYS ADD AT THE END BEFORE THE LAST ONE and NEVER DELETE
//This is due to how Unity saves inspector enum fields (saves an int underneath, and so adding in between might cause inconsistencies)
public enum NotificationType
{
	ConnectivityChange,
	ResetCameraPosition
}