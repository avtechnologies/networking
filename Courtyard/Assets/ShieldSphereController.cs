using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class ShieldSphereController : DamageableNetworkObject, IPowerUpAdapter {

	private Vector3 sphereScale;
	private ParticleSystem[] PS;
	private Collider shieldCollider;
	private bool isShieldUp = false;
	private const float MAX_SHIELD = 100f;
	private float currentShieldStrenght =0;
	public PlayerUIController uiController;

	void Awake()
	{
		shieldCollider = GetComponent<Collider>();
		PS = GetComponentsInChildren<ParticleSystem>();
		sphereScale = transform.localScale;
		ToggleEmission(false);
		transform.localScale = Vector3.zero;
		shieldCollider.enabled = false;
		uiController.ToggleShieldBar(false,0);
	}

	[PunRPC]
	public void Activate()
	{
		HOTween.To(transform,1f,new TweenParms().Prop("localScale",sphereScale,false).Ease(EaseType.EaseOutBounce).OnComplete(()=>{
			ToggleEmission(true);
			shieldCollider.enabled = true;
			isShieldUp = true;
			currentShieldStrenght = MAX_SHIELD;
			uiController.ToggleShieldBar(true,currentShieldStrenght/MAX_SHIELD);
		}));
	}

	public void Deactivate()
	{
		ToggleEmission(false);
		shieldCollider.enabled = false;
		isShieldUp = false;
		uiController.ToggleShieldBar(false,currentShieldStrenght/MAX_SHIELD);
		
		HOTween.To(transform,1f,new TweenParms().Prop("localScale",Vector3.zero,false).Ease(EaseType.EaseInQuad).OnComplete(()=>{
		}));
	}

	private void ToggleEmission(bool isEnabled)
	{
		foreach(ParticleSystem p in PS)
		{
			p.enableEmission = isEnabled;
		}
	}

	private void Update()
	{
		if(isShieldUp)
		{
			uiController.ToggleShieldBar(true,currentShieldStrenght/MAX_SHIELD);
		}

		if(photonView.isMine)
		{
			if(Input.GetKeyDown(KeyCode.P))
			{
				photonView.RPC("Activate",PhotonTargets.AllBuffered);
			}
		}
	}

	#region IPowerUpAdapter implementation

	public void ApplyPowerUp (Hashtable parameters)
	{
		photonView.RPC("Activate",PhotonTargets.AllBuffered);
	}

	public bool CanPowerUpBeApplied {
		get {
			return !isShieldUp;
		}
	}

	public PowerUpId id {
		get {
			return PowerUpId.Shield;
		}
	}

	#endregion

	public PhotonView pView {
		get {
			return photonView;
		}
	}

	[PunRPC]
	public override void ApplyDamage(float damage, PhotonPlayer attacker)
	{
		if(isShieldUp)
		{
			currentShieldStrenght-=damage;
			if(currentShieldStrenght<=0)
			{
				Deactivate();
			}
		}
	}
}
