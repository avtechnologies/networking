﻿using UnityEngine;
using System.Collections;

public abstract class DamageableNetworkObject : Photon.MonoBehaviour {
	public abstract void ApplyDamage(float damage, PhotonPlayer attacker);
}
