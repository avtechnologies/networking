using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponController : Photon.MonoBehaviour {

	private const float SCROLL_SPEED = 20f;
	public Transform weaponPivot;
	public WeaponList weaponList;
	private PlayerVehicle player;
	public List<WeaponBase> weapons = new List<WeaponBase>();
	private bool hasLoadedWeapons= false;
	public WeaponBase CurrentWeapon
	{
		get{
			if(weapons.Count>0)
			{
				return weapons[CurrentWeaponIndex];
			}
			else
			{
				return null;
			}
		}
	}

	public int currentWeaponIndex=-1;
	public int CurrentWeaponIndex
	{
		get{
			return currentWeaponIndex;
		}
		set{
			int newValue = (value>=weapons.Count)?0:((value<0)?weapons.Count-1:value);
			newValue = Mathf.Clamp(newValue,0,weapons.Count);

			if(weapons.Count>0)
			{
				if(currentWeaponIndex!=newValue)
				{
					if(photonView.isMine)
					{
						if(currentWeaponIndex>=0)
						{
							weapons[currentWeaponIndex].OnDeactivateView();
						}
						Debug.Log("Activating " + weapons[newValue].name);
						weapons[newValue].OnActivateView();
					}
				}
			}
			currentWeaponIndex = newValue;
		}
	}

	private void Start()
	{
		player = GetComponent<PlayerVehicle>();
		if(photonView.owner == PhotonNetwork.player)
		{
			Debug.Log(player.photonView.owner.GetTeam() + "   "  + name );
			for (int i = 0; i < weaponList.weapons.Length; i++) {
				WeaponBase cWeapon = weaponList.weapons [i];
				if(cWeapon.teamOwner == player.photonView.owner.GetTeam() || cWeapon.teamOwner == PunTeams.Team.none)
				{
					object[] data = new object[]{photonView.owner};
					PhotonNetwork.Instantiate (cWeapon.name, Vector3.zero, Quaternion.identity, 0,data);
				}
			}
		}
	}

	int indexCounter =0;
	public void RegisterWeapon(WeaponBase weapon)
	{
		weapons.Add(weapon);
		if(weapon.DefaultWeapon)
		{
			Debug.Log("Default Weapon Index = " + indexCounter);
			CurrentWeaponIndex =indexCounter;
		}
		indexCounter++;
	}

	public void ToggleWeapons(bool isEnabled)
	{
		foreach(WeaponBase w in weapons)
		{
			if(!isEnabled)
			{
				w.Stop();
			}
			else
			{
				SetToDefaultWeapon();
			}
		}
	}

	public void SetToDefaultWeapon()
	{
		for (int i = 0; i < weapons.Count; i++) {
			WeaponBase w = weapons [i];
			if (w.DefaultWeapon) {
				CurrentWeaponIndex =i;
			}
		}
	}

	private void Update()
	{
		if(player.photonView.isMine && CurrentWeapon)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				CurrentWeaponIndex++;
				
				while(!CurrentWeapon.IsAvailable())
				{
					CurrentWeaponIndex++;
				}
			}

			if(CurrentWeapon!=null && !player.isDead)
			{
				CurrentWeapon.WeaponUpdate();
			}
		}
	}
}
