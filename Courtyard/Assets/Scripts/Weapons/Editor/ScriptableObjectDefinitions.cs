﻿using UnityEngine;
using UnityEditor;

public class YourClassAsset
{
	[MenuItem("Assets/Create/Weapon List Asset")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<WeaponList> ();
	}

	[MenuItem("Assets/Create/AutoWebBuild Settings")]
	public static void CreateAutoWebBuildAsset ()
	{
		ScriptableObjectUtility.CreateAsset<AutoWebBuildSettings> ();
	}
}

