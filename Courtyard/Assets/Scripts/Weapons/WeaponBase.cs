﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class WeaponBase : Photon.MonoBehaviour {
	public abstract bool DefaultWeapon{get;}
	public abstract void WeaponUpdate();
	public abstract void OnActivateView();
	public abstract void OnDeactivateView();
	public abstract void Stop();
	public abstract bool IsAvailable();
	protected PlayerVehicle playerVehicle;
	protected WeaponController weaponController;
	public Vector3 gunPivotOffset = Vector3.zero;
	public PunTeams.Team teamOwner;

	protected virtual void Awake()
	{
		PhotonPlayer owner = (PhotonPlayer)photonView.instantiationData[0];
		playerVehicle = NetworkConnectionController.GetVehicle(owner);
		weaponController = playerVehicle.GetComponent<WeaponController>();
		transform.parent = weaponController.weaponPivot;
		transform.localPosition = Vector3.zero + gunPivotOffset;;
		transform.localRotation = Quaternion.identity;
		if(photonView.isMine)
		{
			OnDeactivateView();
		}
		weaponController.RegisterWeapon(this);
	}
}
