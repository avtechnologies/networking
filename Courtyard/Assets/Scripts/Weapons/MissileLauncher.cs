using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class MissileLauncher : WeaponBase, IPowerUpAdapter {

	private const float MISSILE_COOLTIME = 1f;
	private const float MAX_CAMERA_HEIGHT = 1500f;
	private int missileCount=0;
	private float currentCoolTime =0;
	private Tweener cameraTween;

	public int MissileCount
	{
		get{
			return missileCount;
		}
		set{
			missileCount = Mathf.Clamp(value,0,3);
			GamePlayScreen.instance.UpdateMissileIcons(missileCount);
			if(missileCount==0)
			{

				GetComponentInParent<WeaponController>().SetToDefaultWeapon();
			}
		}
	}

	public float CameraYPos
	{
		get{
			return Camera.main.transform.position.y;
		}
		set{
			Vector3 p = Camera.main.transform.position;
			Camera.main.transform.position = new Vector3(p.x,value,p.z);
		}
	}

	protected override void Awake ()
	{
		cameraTween = HOTween.To(this,1.5f,new TweenParms().Prop("CameraYPos",MAX_CAMERA_HEIGHT,false).Ease(EaseType.EaseInOutQuad).AutoKill(false).Pause());
		base.Awake ();
	}

	public override void WeaponUpdate ()
	{
		if(photonView.isMine)
		{
			RadarSystem.instance.UpdateMissileLock();
			if(Input.GetMouseButtonDown(0) && currentCoolTime<Time.time && !GetComponentInParent<PlayerVehicle>().isDead)
			{
				if( RadarSystem.instance.IsLockedOnTarget)
				{
					currentCoolTime = Time.time + MISSILE_COOLTIME;
					MissileCount--;
					PhotonPlayer targetPlayer = RadarSystem.instance.currentTarget.photonView.owner;
					PhotonNetwork.Instantiate("Missile",transform.position,transform.rotation,0, new object[]{targetPlayer});
				}
			}
		}
	}

	public override void OnActivateView ()
	{
		Debug.Log("Activating Missile Launcher View");
		UIController.ChangePanel(HTSequenceId.TargetingScreen);
		GamePlayScreen.instance.UpdateMissileIcons(missileCount);
		GamePlayScreen.instance.ToggleMissileView(true);
		cameraTween.PlayForward();
	}

	public override void OnDeactivateView ()
	{
		if(!playerVehicle.isDead)
		{
			UIController.ChangePanel(HTSequenceId.GamePlayScreen);
		}
		GamePlayScreen.instance.UpdateMissileIcons(missileCount);
		GamePlayScreen.instance.ToggleMissileView(false);
		cameraTween.PlayBackwards();
	}

	public override bool IsAvailable ()
	{
		return MissileCount>0;
	}

	public override bool DefaultWeapon {
		get {
			return false;
		}
	}


	public void ApplyPowerUp (Hashtable parameters)
	{
		MissileCount += (int)parameters["missileCount"];
	}

	public PowerUpId id {
		get {
			return  PowerUpId.Missiles;
		}
	}

	public bool CanPowerUpBeApplied {
		get {
			return !(MissileCount==3);
		}
	}

	public override void Stop ()
	{
	}
}
