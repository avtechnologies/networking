using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Vectrosity;

public class LaserCannon : WeaponBase {

	private ParticleSystem[] pSystems;
	private float maxLightIntensity;
	private ParticleCollisionEvent[] collisionEvents;
	private float elapsedCollisionTime;
	private float elapsedExplosionTime;
	private float damagePerHit = 20f;
	private bool isFiring =false;
	
	public Light fireLight;
	public float lightFrequency = 10f;
	public ParticleSystem laser;
	public ParticleSystem[] smallExplosions;
	public AudioSource miniGunClip;
	public AudioSource miniGunStopClip;
	public AudioSource gunOverheat;
	public const float MIN_COLLISION_RATE=0.08f;
	public const float MIN_EXPLOSION_RATE=0.02f;
	private const float LASER_MAX_HEAT = 4f;
	private float laserTemperature=0;
	
	protected override void Awake ()
	{
		base.Awake ();
		pSystems = GetComponentsInChildren<ParticleSystem>();
		bool isPurpleTeam = (photonView.owner.GetTeam() == PunTeams.Team.purple);
		maxLightIntensity = fireLight.intensity;
		fireLight.enabled = false;
		collisionEvents = new ParticleCollisionEvent[10];
		elapsedCollisionTime = Time.time;

	}

	[PunRPC]
	public void ToggleFire(bool isEnabled)
	{
		isFiring = isEnabled;
		if (isEnabled) {
			miniGunClip.Play ();
		} else {
			miniGunClip.Stop();
			miniGunStopClip.Play();
		}
		fireLight.enabled = isEnabled;
		
		foreach(ParticleSystem p in pSystems)
		{
			if(isEnabled)
			{
				p.Simulate(0.01f,true);
				p.enableEmission=true;
				p.Play();
			}else
			{
				p.enableEmission=false;
				p.Stop();
			}
		}
	}



	
	void OnParticleCollision(GameObject go)
	{
		if((Time.time - elapsedExplosionTime)>=MIN_EXPLOSION_RATE)
		{
			laser.GetCollisionEvents (go, collisionEvents);
			if(collisionEvents.Length>0)
			{
				int index =0;
				ParticleCollisionEvent e = collisionEvents[index];
				while(e.intersection==Vector3.zero && index<collisionEvents.Length)
				{
					index++;
					e = collisionEvents[index];
				}
				ParticleSystem g = Instantiate(smallExplosions[ Random.Range(0,smallExplosions.Length)],e.intersection,Quaternion.identity) as ParticleSystem;
				g.transform.rotation = Quaternion.FromToRotation(g.transform.up,e.normal);
				g.Simulate(0.01f);
				g.Play();
			}
		}
		elapsedExplosionTime = Time.time;

		//Only process collisions that are set apart with an interval defined by MIN_COLLISION_RATE
		if((Time.time - elapsedCollisionTime)<MIN_COLLISION_RATE)
		{
			return;
		}
		elapsedCollisionTime = Time.time;

		if(playerVehicle.photonView.isMine)
		{
			CollisionDetector other = go.GetComponentInParent<CollisionDetector>();
			if(other!=null && other.damageableObject.photonView.owner.GetTeam()!=GetComponentInParent<PlayerVehicle>().photonView.owner.GetTeam())
			{
				other.damageableObject.photonView.RPC ("ApplyDamage", PhotonTargets.All, damagePerHit, PhotonNetwork.player);
			}
		}
	}

	#region IWeapon implementation

	public bool canFire = true;
	public override void WeaponUpdate ()
	{
		if(photonView.isMine)
		{
			if(Input.GetMouseButtonDown(0) && canFire)
			{
				photonView.RPC("ToggleFire",PhotonTargets.AllBuffered,true);
			}
			
			if(Input.GetMouseButtonUp(0))
			{
				photonView.RPC("ToggleFire",PhotonTargets.AllBuffered,false);
			}
			
			if(isFiring)
			{
				laserTemperature+=Time.deltaTime;
				if(laserTemperature>LASER_MAX_HEAT)
				{
					GamePlayScreen.instance.gunTGauge.StartAlert();
					gunOverheat.Play();
					canFire=false;
					photonView.RPC("ToggleFire",PhotonTargets.All,false);
				}
			}
			else 
			{
				laserTemperature-=Time.deltaTime;
				if(!canFire)
				{
					if(laserTemperature<LASER_MAX_HEAT)
					{
						GamePlayScreen.instance.gunTGauge.StopAlert();
						canFire = true;
					}
				}
			}
			
			laserTemperature = Mathf.Clamp(laserTemperature,0,LASER_MAX_HEAT);
			GamePlayScreen.instance.gunTGauge.SetProgressBarValue(laserTemperature/LASER_MAX_HEAT);
		}
		if(fireLight.enabled)
		{
			fireLight.intensity = ((Mathf.Sin(Time.time * lightFrequency )/2f)+0.5f) * maxLightIntensity;
		}
	}

	public override void OnActivateView ()
	{
		GamePlayScreen.instance.ToggleLaserCannonView(true);
	}

	public override void OnDeactivateView ()
	{
		GamePlayScreen.instance.ToggleLaserCannonView(false);
	}

	public override bool IsAvailable ()
	{
		return true;
	}

	public override bool DefaultWeapon {
		get {
			return true;
		}
	}

	public override void Stop ()
	{
		if(isFiring)
		{
			isFiring=false;
			photonView.RPC("ToggleFire",PhotonTargets.All,false);
		}
	}
	#endregion
}
