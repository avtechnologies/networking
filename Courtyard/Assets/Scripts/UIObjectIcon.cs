﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public enum UIIconId
{
	Friendly,
	Enemy,
	MissilePowerUp,
	ShieldPowerUp
}

public enum UIIconVisibilityOptions
{
	AlwaysVisible,
	VisibleWhenObjectIsVisible,
	InvisibleWhenObjectIsVisible
}

[System.Serializable]
public class UICanvasGroupVisibilityInfo
{
	public CanvasGroup canvasGroup;
	public UIIconVisibilityOptions option;
}


[RequireComponent(typeof(CanvasGroup))]
public class UIObjectIcon : MonoBehaviour {

	[HideInInspector]
	public GameObject WorldObject;
	public RectTransform UI_Element;
	public Canvas canvas;

	protected float xLimitOffset;
	protected float yLimitOffset;
	public int poolSize = 20;
	public UIIconId iconId;

	public UICanvasGroupVisibilityInfo[] canvasGroups;
	public Vector2 ViewportPosition;


	protected virtual void LateUpdate () {
		//this is your object that you want to have the UI element hovering over
		Camera mapCamera = Camera.main;
		RectTransform CanvasRect=canvas.GetComponent<RectTransform>();
		if (WorldObject == null) {
			UIObjectIconManager.instance.UnsetIcon(this);
			return;
		}

		ViewportPosition = mapCamera.WorldToViewportPoint (WorldObject.transform.position);

		Vector2 pos=new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));


		xLimitOffset = UI_Element.sizeDelta.x/2f;
		yLimitOffset = UI_Element.sizeDelta.y/2f;

		float xMin = -(CanvasRect.sizeDelta.x * 0.5f)+ xLimitOffset;
		float xMax = (CanvasRect.sizeDelta.x * 0.5f)- xLimitOffset;
		float yMin = -(CanvasRect.sizeDelta.y * 0.5f) + yLimitOffset;
		float yMax = (CanvasRect.sizeDelta.y * 0.5f) - yLimitOffset;


		foreach (UICanvasGroupVisibilityInfo c in canvasGroups) {
			switch (c.option) {
			case UIIconVisibilityOptions.AlwaysVisible:
				c.canvasGroup.alpha = 1f;
				break;
			case UIIconVisibilityOptions.InvisibleWhenObjectIsVisible:
				c.canvasGroup.alpha = (( pos.x<xMin || pos.x>xMax) || ( pos.y<yMin || pos.y>yMax) )?1f:0;
				break;
			case UIIconVisibilityOptions.VisibleWhenObjectIsVisible:
				c.canvasGroup.alpha = (( pos.x>xMin && pos.x<xMax) && ( pos.y>yMin && pos.y<yMax) )?1f:0;
				break;
			}
		}

		pos = new Vector2 (Mathf.Clamp(pos.x,xMin,xMax),Mathf.Clamp(pos.y,yMin,yMax));
		UI_Element.anchoredPosition=pos;
	}

	public virtual void ToggleVisibility(bool isEnabled)
	{
		GetComponent<CanvasGroup> ().alpha = (isEnabled)?1f:0;
	}
}
