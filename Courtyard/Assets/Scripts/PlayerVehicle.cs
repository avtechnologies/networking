﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class PlayerVehicle : DamageableNetworkObject {

//	public const float MINIMUM_ROTATION_MOUSE_DISTANCE = 120f;
//	public const float MIN_SPEED=200f;
//	public const float MAX_SPEED=600f;
//	public const float NITRO_SPEED=1400f;
//	public const float ROTATION_SPEED = 15f;
//	public const float ACCELERATOR_MOUSE_DISTANCE= 350f;
//	public const float ENGINE_MAX_HEAT = 3f;
//	public const float MAX_BANK_SPEED = 40f;
//	public const float MAX_HEALTH =100f;

	[HideInInspector]
	public bool scoreGiven=false;

	public AnimationCurve speedDistanceRate;
	public FracturedObjectController explosion;
	[HideInInspector]
	public float CurrentSpeed
	{
		get{
			return currentSpeed;
		}
		internal set{
			currentSpeed = value;
		}
	}
	[HideInInspector]
	public CharacterController characterController;
	[HideInInspector]
	public UIObjectIcon icon;

	public float accelerationRate;
	public ParticleSystem[] exhaustPS;
	public float engineTemperature=0;
	public bool isEngineOverheat = false;
	public ParticleSystem nitroResistance;
	public AudioSource afterBurner;
	public AudioSource engineSound;
	public TrailRenderer leftTrail;
	public TrailRenderer rightTrail;
	public Transform meshTransform;
	public Light exhaustLight;
	public float minP = float.MinValue;
	public float maxP = float.MaxValue;
	[HideInInspector]
	public PlayerUIController playerUIController;

	private float currentSpeed;
	private Material trailMaterial;
	private float bankSpeed =0;
	private Quaternion lookAtRotation;
	private Quaternion nextRotation;
	private bool nitroActivated = false;
	private bool lockPlayer =false;
	private Vector3 direction;
	public float percentageSpeed;

	public bool isDead = false;
	public float health = Parameters.MAX_HEALTH;
	public float Health
	{
		get{
			return health;
		}
		internal set{
			health =  Mathf.Clamp(value,0,Parameters.MAX_HEALTH);
			playerUIController.healthSlider.value = health / Parameters.MAX_HEALTH;
		}
	}
	[HideInInspector]
	public Vector3 velocity;

	public AnimationCurve rotationSpeedActivationCurve;
	private PhotonTransformView pTransformView;

	private void Awake()
	{
		pTransformView = GetComponent<PhotonTransformView>();
		characterController = GetComponent<CharacterController>();
		UIIconId iconId = UIIconId.Friendly;
		if (photonView.owner.GetTeam()!=PhotonNetwork.player.GetTeam()) {
			iconId = UIIconId.Enemy;
		}
		else
		{
			iconId = UIIconId.Enemy;
		}
		icon = UIObjectIconManager.instance.SetIcon<UIObjectIcon> (iconId,gameObject);
		trailMaterial = Instantiate<Material> (leftTrail.material);
		leftTrail.material = trailMaterial;
		rightTrail.material = trailMaterial;
		playerUIController = gameObject.GetComponentInChildren<PlayerUIController> ();
		RadarSystem.instance.RegisterVehicle(this);
		NotificationCenter.Post(NotificationType.ResetCameraPosition,this);
	}
	

	private void ApplyNitro ()
	{
		if (vmouse>=1f && !isEngineOverheat) {
			if (!nitroActivated) {
				photonView.RPC ("ApplyNitroNetworkBroadCast", PhotonTargets.All, true);
			}
		}
		else {
			if (nitroActivated) {
				photonView.RPC ("ApplyNitroNetworkBroadCast", PhotonTargets.All, true);
			}
		}
		if (vmouse<1f) {
			photonView.RPC ("ApplyNitroNetworkBroadCast", PhotonTargets.All, false);
		}

		if (nitroActivated) {
			engineTemperature += Time.deltaTime;
			if (engineTemperature >= Parameters.ENGINE_MAX_HEAT && !isEngineOverheat) {
				GamePlayScreen.instance.engineTGauge.StartAlert ();
				isEngineOverheat = true;
				photonView.RPC ("ApplyNitroNetworkBroadCast", PhotonTargets.All, false);
			}
		}
		else {
			engineTemperature -= Time.deltaTime;
		}
		engineTemperature = Mathf.Clamp (engineTemperature, 0, Parameters.ENGINE_MAX_HEAT);
		if (isEngineOverheat) {
			if (engineTemperature == 0) {
				isEngineOverheat = false;
				GamePlayScreen.instance.engineTGauge.StopAlert ();
			}
		}
		GamePlayScreen.instance.engineTGauge.SetProgressBarValue (engineTemperature / Parameters.ENGINE_MAX_HEAT);
	}


	public float viewPortDistanceFromCenter;
	public float vmouse;
	public AnimationCurve rotationVersusSpeedRelationship;
	float cMaximumSpeed;

	private void CheckMovement ()
	{
		Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y - transform.position.y));
		Vector3 screenCenter = Camera.main.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, Camera.main.transform.position.y - transform.position.y));
		viewPortDistanceFromCenter = Mathf.Min(Vector3.Distance(Camera.main.ScreenToViewportPoint(Input.mousePosition),new Vector3(0.5f,0.5f))/0.5f,1f);

		vmouse = Mathf.Min(Vector3.Distance(Camera.main.ScreenToViewportPoint(Input.mousePosition),(Vector2)Camera.main.WorldToViewportPoint(transform.position))/0.7f,1f);


		//float mouseDistanceFromCenter = Vector3.Distance (screenCenter, new Vector3 (mouseWorldPos.x, 0, mouseWorldPos.z));
		float mouseDistanceFromVehicle = Vector3.Distance (transform.position, mouseWorldPos);
		direction = mouseWorldPos - transform.position;
		lookAtRotation = Quaternion.LookRotation (direction, Vector3.up);
		float currentRotationSpeed =    rotationVersusSpeedRelationship.Evaluate(CurrentSpeed/Parameters.NITRO_SPEED) * Parameters.ROTATION_SPEED;//     (CurrentSpeed<=Parameters.MAX_SPEED)?Parameters.MINIMUM_ROTATION_MOUSE_DISTANCE:Parameters.NITRO_ROTATION_SPEED;
		nextRotation = Quaternion.Lerp (transform.rotation, lookAtRotation, rotationSpeedActivationCurve.Evaluate (Mathf.Min (mouseDistanceFromVehicle, Parameters.MINIMUM_ROTATION_MOUSE_DISTANCE) / Parameters.MINIMUM_ROTATION_MOUSE_DISTANCE) * currentRotationSpeed * Time.deltaTime);


		float yCross = Mathf.Round (Vector3.Cross (transform.rotation * Vector3.forward, nextRotation * Vector3.forward).y * 50f) / 50f;
		float bankDir = -((yCross != 0) ? yCross / Mathf.Abs (yCross) : 0f);
		bankSpeed = Mathf.Lerp (bankSpeed, bankDir * Parameters.MAX_BANK_SPEED, 10f * Time.deltaTime);
		meshTransform.localRotation = Quaternion.Euler (0, 0, bankSpeed);


		transform.rotation = nextRotation;
//		accelerationRate = speedDistanceRate.Evaluate ((Mathf.Min (mouseDistanceFromVehicle, Parameters.ACCELERATOR_MOUSE_DISTANCE) / Parameters.ACCELERATOR_MOUSE_DISTANCE));
		accelerationRate = speedDistanceRate.Evaluate (vmouse);
		
		ApplyNitro ();

		cMaximumSpeed = Mathf.Lerp(cMaximumSpeed, ((nitroActivated)?Parameters.NITRO_SPEED:Parameters.MAX_SPEED), 4f * Time.deltaTime);

		CurrentSpeed = Mathf.Lerp (Parameters.MIN_SPEED, cMaximumSpeed, accelerationRate);
		percentageSpeed = CurrentSpeed/cMaximumSpeed;
		velocity = transform.forward * CurrentSpeed;
	}

	private void Update()
	{
		if (photonView.isMine && !isDead) 
		{

			if(Input.GetKeyDown(KeyCode.Q))
			{
				PhotonNetwork.Disconnect();
			}

			if(Input.GetKeyDown(KeyCode.L))
			{
				lockPlayer = !lockPlayer;
			}

			if(Input.GetKeyDown(KeyCode.D))
			{
				Health =0;
			}

			CheckMovement ();


			if(lockPlayer)
			{
				velocity = Vector3.zero;
			}

			characterController.Move (velocity * Time.deltaTime);
			pTransformView.SetSynchronizedValues(velocity,0);
			CheckHealth ();
		}


		float u = Mathf.PerlinNoise (Time.time*5f, 0.5f);
		minP = Mathf.Min (minP, u);
		maxP = Mathf.Max (maxP, u);
		exhaustLight.intensity = Mathf.Lerp(exhaustLight.intensity, ((!nitroActivated)?2f:6f) - (0.2f / u), Time.deltaTime *10f);
		float targetTrailAlpha = (nitroActivated) ? 0.3f : 0f;
		trailMaterial.SetColor("_TintColor",new Color(1f,1f,1f,Mathf.Lerp(trailMaterial.GetColor("_TintColor").a,targetTrailAlpha, Time.deltaTime * 5f)));
		engineSound.pitch = Mathf.Lerp (engineSound.pitch, (nitroActivated) ? 1.6f : 1, Time.deltaTime * 5f);
	}



	[PunRPC]
	public void ApplyNitroNetworkBroadCast(bool hasNitro)
	{
		nitroActivated = hasNitro;
		if (hasNitro) {
			afterBurner.Play();
			nitroResistance.Play ();
			nitroResistance.enableEmission=true;
		} else {
			nitroResistance.enableEmission=false;
		}
	}

	[PunRPC]
	public override void ApplyDamage(float damage, PhotonPlayer attacker)
	{
		Health-=damage;
		healthIncrementCounter = Time.time+1f;
		if (photonView.owner == PhotonNetwork.player) {
			if(Health<=0 && !scoreGiven)
			{
				attacker.SetScore(attacker.GetScore()+1);
				scoreGiven = true;
			}
		}
	}

	private float healthIncrementCounter = 0;
	private void CheckHealth ()
	{
		if (Health <= 0 && !isDead) {
			photonView.RPC("ToggleVisibility",PhotonTargets.AllBuffered,false);

			if (photonView.isMine) {
				UIController.ChangePanel (HTSequenceId.SpawnScreen);
				SpawningScreen.onRespawn = ()=>{
					photonView.RPC("OnRespawn",PhotonTargets.All,NetworkConnectionController.GetRandomSpawnPosition());
				};
			}
		}
		else
		{
			if(Time.time>healthIncrementCounter)
			{
				if(Health<Parameters.MAX_HEALTH)
				{
					healthIncrementCounter = Time.time + 0.1f;
					Health +=1f;
					photonView.RPC ("ReplenishHealth",PhotonTargets.AllBuffered, Health);
				}
			}
		}
	}


	[PunRPC]
	public void ReplenishHealth(float newHealth)
	{
		Health = newHealth;
	}

	[PunRPC]
	public void ToggleVisibility(bool isEnabled)
	{
		isDead = !isEnabled;
		if(isEnabled)
		{
			RadarSystem.instance.RegisterVehicle (this);
			Health = Parameters.MAX_HEALTH;
			scoreGiven = false;
		}
		else
		{
			FracturedObjectController f = Instantiate (explosion, transform.position, transform.rotation) as FracturedObjectController;
			f.direction = direction;
			f.currentSpeed = CurrentSpeed;
			RadarSystem.instance.UnregisterVehicle (this);
			if(nitroActivated)
			{
				photonView.RPC("ApplyNitroNetworkBroadCast",PhotonTargets.All,false);
			}
		}
		exhaustLight.enabled = isEnabled;
		foreach (var particleSystem in exhaustPS) {
			particleSystem.enableEmission = isEnabled;
		}
		meshTransform.gameObject.GetComponent<Renderer>().enabled =isEnabled;
		gameObject.GetComponent<CharacterController>().enabled =isEnabled;
		meshTransform.gameObject.GetComponent<MeshCollider>().enabled =isEnabled;
		gameObject.GetComponent<WeaponController>().ToggleWeapons(isEnabled);
		Debug.Log(playerUIController + "   "  +  name);
		playerUIController.gameObject.SetActive(isEnabled);
		icon.gameObject.SetActive(isEnabled);
	}

	[PunRPC]
	public void OnRespawn (Vector3 spawnPosition)
	{
		transform.position = spawnPosition;
		NotificationCenter.Post(NotificationType.ResetCameraPosition,this);
		photonView.RPC("ToggleVisibility",PhotonTargets.AllBuffered,true);
	}

	private void OnDestroy()
	{
		RadarSystem.instance.UnregisterVehicle(this);
	}
}
