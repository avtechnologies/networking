﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUIController : MonoBehaviour {

	private PlayerVehicle vehicle;
	public Slider healthSlider;
	public Slider shieldSlider;
	public Text nameText;
	public Image missileLockBG;
	public Image missileLockSlider;

	private Vector3 missileLockScale;

	void Awake()
	{
		vehicle = GetComponentInParent<PlayerVehicle> ();
		missileLockBG.enabled = false;
		missileLockScale = missileLockBG.transform.localScale;
	}

	void Start()
	{
		if (vehicle.photonView.isMine) {
			nameText.enabled = false;
		} else {
			nameText.text = vehicle.photonView.owner.name;
		}
	}

	public void ToggleMissileLock(bool isEnabled)
	{
		missileLockBG.enabled = isEnabled;
		missileLockSlider.enabled = isEnabled;
		if(!isEnabled)
		{
			missileLockBG.transform.localScale = missileLockScale;
			missileLockSlider.fillAmount =0;
		}
	}
		
	public void SetMissileLockProgress(float percentage)
	{
		missileLockSlider.fillAmount = percentage;
	}

	public void ToggleShieldBar(bool isEnabled, float percentage)
	{
		if(isEnabled!=shieldSlider.gameObject.activeSelf)
		{
			shieldSlider.gameObject.SetActive(isEnabled);
		}
		if(isEnabled)
		{
			shieldSlider.value = percentage;
		}
	}

	// Update is called once per frame
	void Update () {

		transform.rotation = Quaternion.Euler (0, 0, 0);

		if(missileLockBG.enabled && missileLockSlider.fillAmount>=1)
		{
			missileLockBG.transform.localScale = missileLockScale * ( 1f +  (Mathf.Sin(Time.time * 20f)/2f +0.5f)*0.2f  );
		}
	}
}
