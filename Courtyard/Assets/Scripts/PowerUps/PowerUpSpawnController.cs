﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class LootTableItem
{
	public string resourceName;
	public int concurrentAmount;
	public PowerUpId id;
	public int amountInScene;
}

public class PowerUpSpawnController : Photon.MonoBehaviour {

	public LootTableItem[] lootItems;
	Dictionary<PowerUpId,LootTableItem> lootTableDict = new Dictionary<PowerUpId, LootTableItem>();
//	public static PowerUpSpawnController instance;


	void Awake()
	{
//		instance = this;
		foreach(LootTableItem l in lootItems)
		{
			lootTableDict.Add(l.id,l);
		}
	}

	void OnMasterClientSwitched ( PhotonPlayer newMasterClient )
	{
		if (PhotonNetwork.isMasterClient) {
			PhotonNetwork.OnEventCall += HandlePickedUpPowerup;
		}
	}

	void OnJoinedRoom()
	{
		PhotonNetwork.OnEventCall += HandlePickedUpPowerup;
		if(PhotonNetwork.isMasterClient)
		{
			StartCoroutine(CreatePowerUpsCoroutine());
		}
	}

	void HandlePickedUpPowerup (byte eventCode, object content, int senderId)
	{
		if(eventCode == (byte)NetworkEventCodes.PickedPowerUp)
		{
			PowerUp powerUp =  PhotonView.Find((int)content).GetComponent<PowerUp>();
			PhotonNetwork.Destroy(powerUp.gameObject);
		}
	}

	IEnumerator CreatePowerUpsCoroutine()
	{
		while(true)
		{
			yield return new WaitForSeconds(Random.Range(1f,3f));
			foreach(KeyValuePair<PowerUpId,LootTableItem> kv in lootTableDict)
			{
				kv.Value.amountInScene =0;
			}
			PowerUp[] pUps = FindObjectsOfType<PowerUp>();
			foreach(PowerUp p in pUps)
			{
				lootTableDict[p.id].amountInScene++;
			}

			foreach(LootTableItem i in lootItems)
			{
				for(int p=0;p<(i.concurrentAmount - i.amountInScene);p++)
				{
					PhotonNetwork.InstantiateSceneObject(i.resourceName,NetworkConnectionController.GetRandomSpawnPosition(),Quaternion.identity,0,new object[]{0});
				}
			}
		}
	}

	void OnDisconnectedFromPhoton ( )
	{
		PhotonNetwork.OnEventCall -= HandlePickedUpPowerup;
	}

	void OnDestroy()
	{
		PhotonNetwork.OnEventCall -= HandlePickedUpPowerup;
	}
}
