using UnityEngine;
using System.Collections;

public enum PowerUpId
{
	Missiles,
	Shield
}

public interface IPowerUpAdapter
{
	void ApplyPowerUp(Hashtable parameters);
	bool CanPowerUpBeApplied {get;}
	PowerUpId id {get;}
}

[RequireComponent(typeof(PhotonView))]
public abstract class PowerUp : Photon.MonoBehaviour {

	public UIIconId iconID;
	public PowerUpId id;
	[HideInInspector]
	public UIObjectIcon icon;
	public abstract Hashtable Parameters{get; }
	// Use this for initialization

	private void Start()
	{
		photonView.ownershipTransfer = OwnershipOption.Takeover;
		icon = UIObjectIconManager.instance.SetIcon<UIObjectIcon> (iconID,gameObject);
	}

	protected virtual void OnTriggerEnter(Collider other)
	{
		PlayerVehicle player = other.GetComponentInParent<PlayerVehicle>();
		if(player!=null && player.photonView.owner == PhotonNetwork.player)
		{
			IPowerUpAdapter[] powerUpAdapters =  player.GetComponentsInChildren<IPowerUpAdapter>();
			foreach(IPowerUpAdapter p in powerUpAdapters)
			{
				if(id == p.id && p.CanPowerUpBeApplied)
				{
					p.ApplyPowerUp(Parameters);
					Debug.Log("Picked Power Up");
					PhotonNetwork.RaiseEvent((byte)NetworkEventCodes.PickedPowerUp,photonView.viewID,true,new RaiseEventOptions(){ Receivers = ExitGames.Client.Photon.ReceiverGroup.MasterClient});
				}
			}
		}
	}

	private void OnDestroy()
	{
		UIObjectIconManager.instance.UnsetIcon(icon);
	}
}
