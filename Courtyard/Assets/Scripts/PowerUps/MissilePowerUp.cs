﻿using UnityEngine;
using System.Collections;

public class MissilePowerUp : PowerUp {
	#region implemented abstract members of PowerUp

	public override Hashtable Parameters {
		get {
			Hashtable hash = new Hashtable();
			hash.Add("missileCount",3);
			return hash;
		}
	}

	#endregion
}
