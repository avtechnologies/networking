using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum NetworkEventCodes
{
	PickedPowerUp =0
}

public class NetworkConnectionController : Photon.MonoBehaviour {

	public string roomName="Hell";

	public int ping;
	private static NetworkConnectionController instance;

	public Action joinedLobbyAction;
	public Action joinedRoomAction;

	void Awake()
	{
		instance = this;
	}

	public static void Connect(string settings, Action joinedLobbyAction)
	{
		instance.joinedLobbyAction = joinedLobbyAction;
	}

	void OnJoinedLobby()
	{
		if(joinedLobbyAction!=null)
		{
			joinedLobbyAction();
			joinedLobbyAction = null;
		}
	}

	public static void JoinRoom(string nick, Action joinedRoomAction)
	{
		instance.joinedRoomAction = joinedRoomAction;
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.isVisible = true;
		roomOptions.isOpen = true;
		TypedLobby typedLobby = new TypedLobby();
		PhotonNetwork.player.name = nick;
		PhotonNetwork.JoinOrCreateRoom(instance.roomName,roomOptions,typedLobby);
	}

	void OnJoinedRoom()
	{
		if(joinedRoomAction!=null)
		{
			joinedRoomAction();
			joinedRoomAction = null;
		}

//		if (PhotonNetwork.isMasterClient) {
//			photonView.RPC("ReceiveSpawnPositions",PhotonTargets.OthersBuffered,GetSerializedNetworkSyncedObjects());
//		}
		SpawnMyPlayerEverywhere(GetRandomSpawnPosition(),GetTeam());
	}



	public static PlayerVehicle GetVehicle(PhotonPlayer player)
	{
		PlayerVehicle[] vehicles = FindObjectsOfType<PlayerVehicle> ();
		foreach (PlayerVehicle p in vehicles) {
			if(p.photonView.owner == player)
			{
				return p;
			}
		}
		return null;
	}
	
	public static Vector3 GetRandomSpawnPosition()
	{
		SpawnPosition[] spawnPts = FindObjectsOfType<SpawnPosition>();
		Vector3 result =spawnPts[0].transform.position;

		List<SpawnPosition> vacantPositions = new List<SpawnPosition>();
		foreach(SpawnPosition s in spawnPts)
		{
			if(!s.CheckSpace())
			{
				vacantPositions.Add(s);
			}
		}

		if(vacantPositions.Count>0)
		{
			result =vacantPositions[UnityEngine.Random.Range(0,vacantPositions.Count)].transform.position;
		}

		return new Vector3(result.x,0,result.z);
	}

	private PunTeams.Team GetTeam()
	{
		PunTeams.Team firstSelection = PunTeams.Team.white;

		if(PunTeams.PlayersPerTeam[PunTeams.Team.white].Count>PunTeams.PlayersPerTeam[PunTeams.Team.purple].Count)
		{
			firstSelection = PunTeams.Team.purple;
		}

		return firstSelection;
	}

	void OnMasterClientSwitched ( PhotonPlayer newMasterClient )
	{
		if (PhotonNetwork.isMasterClient) {
//			photonView.RPC("ReceiveSpawnPositions",PhotonTargets.OthersBuffered,GetSerializedNetworkSyncedObjects());
		}
	}

	void SpawnMyPlayerEverywhere(Vector3 spawnPosition, PunTeams.Team teamId)
	{
		PhotonNetwork.player.SetScore(0);
		PhotonNetwork.player.SetTeam(teamId);
		
		string prefabName = (teamId == PunTeams.Team.white)?"playerWhite":"playerPurple";
		GameObject playerObj = PhotonNetwork.Instantiate(prefabName,spawnPosition,Quaternion.identity,0);
		AudioListener a = FindObjectOfType<AudioListener> ();
		if (a != null) {
			Destroy(a);
		}

		playerObj.AddComponent<AudioListener> ();
	}

//	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
//	{
//	}



	private  void Update()
	{
		ping = PhotonNetwork.GetPing();
	}
}
