using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[CustomEditor(typeof(NetworkTransformConfiguration))]
public class NetworkTransformEditor : Editor {

	public override void OnInspectorGUI ()
	{
		NetworkTransformConfiguration v = (NetworkTransformConfiguration)target;
		if(v.GetComponent<PhotonView>()==null)
		{
			PhotonTransformView ptv = v.gameObject.AddComponent<PhotonTransformView>();
			PhotonView pv = v.gameObject.AddComponent<PhotonView>();
			pv.ObservedComponents = new System.Collections.Generic.List<Component>();
			pv.ObservedComponents.Add(ptv);
			pv.synchronization = ViewSynchronization.Unreliable;
		}
	}
}
