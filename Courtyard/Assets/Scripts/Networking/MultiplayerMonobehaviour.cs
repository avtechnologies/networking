using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;


[RequireComponent(typeof(NetworkTransformConfiguration))]
public abstract class MultiplayerMonobehaviour : DamageableNetworkObject  {
	
	private Vector3 latestCorrectPos;
	private Vector3 onUpdatePos;
	
	private Quaternion latestCorrectRot;
	private Quaternion onUpdateRot;
	private float fraction;
	private PhotonTransformView photonTransformView;

	protected virtual void Awake()
	{
		photonTransformView = GetComponent<PhotonTransformView>();
		latestCorrectPos = transform.position;
		onUpdatePos = transform.position;
		
		latestCorrectRot = transform.rotation;
		onUpdateRot = transform.rotation;
	}

	protected virtual void Synchronize()
	{
	}
}
