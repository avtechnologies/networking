﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RadarSystem : MonoBehaviour {

	public List<PlayerVehicle> enemyList = new List<PlayerVehicle>();
	public List<PlayerVehicle> friendList = new List<PlayerVehicle>();
	public List<PlayerVehicle> anonymousList = new List<PlayerVehicle>();
	private PlayerVehicle player;
	public static RadarSystem instance;

	public const float MINIMUM_LOCK_DISTANCE = 3000f;
	public const float MINIMUM_LOCK_ANGLE = 65f;
	public const float LOCK_TIME = 0.1f;

	[HideInInspector]
	public bool IsLockedOnTarget
	{
		get{
			return currentTarget!=null && targetLockProgress>=1f;
		}
	}
	
	public float targetAcquistionTime =0;
	void Awake()
	{
		instance = this;
	}

	public void SortAnonymousList()
	{
		foreach(PlayerVehicle p in anonymousList)
		{
			if(p.photonView.owner.GetTeam() == player.photonView.owner.GetTeam())
			{
				friendList.Add(p);
			}else
			{
				enemyList.Add(p);
			}
		}
		anonymousList.Clear();
	}

	public void RegisterVehicle(PlayerVehicle vehicle)
	{
		if(vehicle.photonView.isMine)
		{
			player = vehicle;
			SortAnonymousList();
			return;
		}

		if(player==null)
		{
			anonymousList.Add(vehicle);
		}
		else
		{
			if(vehicle.photonView.owner.GetTeam() == player.photonView.owner.GetTeam())
			{
				friendList.Add(vehicle);
			}else
			{
				enemyList.Add(vehicle);
			}
		}
	}

	public void UnregisterVehicle(PlayerVehicle vehicle)
	{
		if(friendList.Contains(vehicle))
		{
			friendList.Remove(vehicle);
		}

		if(enemyList.Contains(vehicle))
		{
			enemyList.Remove(vehicle);
		}
	}

	private class MissileTarget
	{
		public PlayerVehicle vehicle;
		public float distance;
	}

	public PlayerVehicle currentTarget;

	public void UpdateMissileLock()
	{
		if(currentTarget!=null)
		{
			return;
		}

		List<MissileTarget> potentialTargets = new List<MissileTarget>();
		foreach(PlayerVehicle p in enemyList)
		{
			float d =0;
			if(ValidateMissileTarget(p, out d))
			{
				potentialTargets.Add( new MissileTarget(){vehicle = p, distance = d});
			}
		}

		if(potentialTargets.Count>0)
		{
			potentialTargets.Sort((a,b)=>{ return a.distance.CompareTo(b.distance);});
			currentTarget = potentialTargets[0].vehicle;
			currentTarget.playerUIController.ToggleMissileLock(true);
			targetAcquistionTime =Time.time + LOCK_TIME;
		}
		else
		{
			currentTarget = null;
		}
	}

	private bool ValidateMissileTarget(PlayerVehicle p)
	{
		float d=0;
		return ValidateMissileTarget(p,out d);
	}

	private bool ValidateMissileTarget(PlayerVehicle p, out float distance)
	{
		distance =0;
		if(p!=null)
		{
			Vector3 viewportPosition = Camera.main.WorldToViewportPoint(p.transform.position);
			bool isOnScreen = viewportPosition.x>0 && viewportPosition.x<1f && viewportPosition.y>0 && viewportPosition.y<1f;
			distance = Vector3.Distance(player.transform.position,p.transform.position);
			if(distance<MINIMUM_LOCK_DISTANCE && isOnScreen && !p.isDead)
			{
				Vector3 dir = p.transform.position - player.transform.position;
				float dot =Vector3.Dot(player.transform.forward,dir);
				float theta = Mathf.Rad2Deg * Mathf.Acos(dot/ (dir.magnitude * player.transform.forward.magnitude));
				return (dot>=0 && theta<MINIMUM_LOCK_ANGLE);
			}
		}
		return false;
	}



	public float targetLockProgress;
	private void Update()
	{
		if(currentTarget!=null)
		{
			PlayerUIController pUI = currentTarget.playerUIController;
			if(!ValidateMissileTarget(currentTarget))
			{
				pUI.ToggleMissileLock(false);
				currentTarget = null;
				targetAcquistionTime =0;
				return;
			}
			targetLockProgress = (Time.time -  (targetAcquistionTime-LOCK_TIME))/LOCK_TIME;
			pUI.SetMissileLockProgress(targetLockProgress);
		}
	}
}
