﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VehicleTemperatureGauge : MonoBehaviour {

	public Image background;
	public Image progressBar;
	private Color bStartColor;
	private Color pStartColor;
	public Gradient gradient;

	void Awake()
	{
		bStartColor = background.color;
		pStartColor = progressBar.color;
	}
	// Use this for initialization

	public void SetProgressBarValue(float fraction)
	{
		progressBar.fillAmount = fraction;
		progressBar.color = gradient.Evaluate(fraction);
	}

	public void StartAlert()
	{
		if(isActiveAndEnabled)
		{
			StartCoroutine(AlertOverHeat());
		}
	}

	public void StopAlert()
	{
		StopAllCoroutines();
		background.color= bStartColor;
		progressBar.color = pStartColor;
	}

	IEnumerator AlertOverHeat()
	{
		background.color = new Color(1f,0,0,background.color.a);
		progressBar.color = new Color(1f,0,0,progressBar.color.a);
		
		while(true)
		{
			float minAlpha = 0.3f;
			float a =  minAlpha +   (1-minAlpha) * (Mathf.Sin(Time.time * 10f)/2f + 0.5f);
			background.color = new Color(1f,0,0,a);
			progressBar.color = new Color(1f,0,0,a);
			yield return null;
		}
	}
}
