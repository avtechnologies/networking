﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(HTSequence))]
public class UIController : MonoBehaviour {

	public HTSequence htSequence;

	private static UIController instance;
	public static UIController Instance {
			get {
					if (instance == null) {
							instance = FindObjectOfType<UIController> ();
					}
					return instance;
			}
	}

	public UIPanel[] panels;
	private void Awake ()
	{
			if (instance != null && instance != this) {
					Destroy (this);
					return;
			}
			instance = this;
			htSequence = GetComponent<HTSequence>();
	}
	
	public HTSequenceId currentPanelId;
	public Dictionary<HTSequenceId,UIPanel> panelDictionary = new Dictionary<HTSequenceId, UIPanel>();
	
	// Use this for initialization
	void Start () {
		foreach(UIPanel panel in panels)
		{
			panel.gameObject.SetActive(true);
		}
	
		htSequence.PopulateSequenceDictionary();
		
		foreach(UIPanel panel in panels)
		{
			panelDictionary.Add(panel.panelId,panel);
			panel.gameObject.SetActive(false);
		}
		OpenStartingPanel();
	}
	
	private void OpenStartingPanel()
	{
		Instance.panelDictionary[currentPanelId].Open();
	}
	
	public static void ChangePanel(HTSequenceId panelId)
	{
		ChangePanel(panelId,null);
	}
	
	public static void ChangePanel(HTSequenceId panelId, object data)
	{
		ChangePanel (panelId,data,null);
	}
	
	public static void ChangePanel(HTSequenceId panelId, object data, Action onComplete)
	{
		Instance.panelDictionary[Instance.currentPanelId].Close();
		Instance.currentPanelId = panelId;
		UIPanel nextPanel = Instance.panelDictionary[Instance.currentPanelId];
		nextPanel.OnOpenCompleteAction = onComplete;
		nextPanel.data = data;
		nextPanel.Open();
	}
	
	public static void ClosePanel(HTSequenceId panelId)
	{
		UIPanel panel = Instance.panelDictionary[panelId];
		panel.Close();
	}
	
	public static void CloseCurrentPanel()
	{
		UIPanel panel = Instance.panelDictionary[Instance.currentPanelId];
		panel.Close();
	}
	
}
