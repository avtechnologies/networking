﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(UIController))]
public class UIControllerEditor : Editor {

	
	public override void OnInspectorGUI ()
	{
			UIController controller = (UIController)target;
			
			GUILayout.BeginVertical("box");
			GUILayout.Label("Panels");
			GUILayout.Space(5f);
			foreach(UIPanel panel in controller.panels)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Space(10f);
				if(panel!=null)
				{
					GUILayout.Label(panel.name);
					if(GUILayout.Button("s", GUILayout.MaxWidth(20f)))
					{
						foreach(UIPanel p in controller.panels)
						{
							p.gameObject.SetActive(false);
						}
						panel.gameObject.SetActive(true);
					}
					GUILayout.FlexibleSpace();
				}
				else
				{
					GUILayout.Label("<deleted Panel>");
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Show All"))
			{
				foreach(UIPanel p in controller.panels)
				{
					p.gameObject.SetActive(true);
				}
			}
				
			if(GUILayout.Button("Hide All"))
			{
				foreach(UIPanel p in controller.panels)
				{
					p.gameObject.SetActive(false);
				}
			}
			GUILayout.EndHorizontal();
			
			GUILayout.Space(10f);
			if(GUILayout.Button("Update List"))
			{
				controller.panels = controller.GetComponentsInChildren<UIPanel>();
			}
			
			controller.currentPanelId =  (HTSequenceId)EditorGUILayout.EnumPopup("Start Panel",controller.currentPanelId);
	}
}
