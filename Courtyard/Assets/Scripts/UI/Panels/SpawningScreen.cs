﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SpawningScreen : UIPanel {

	public Text txtTimer;
	public Text txtReadyToPlay;
	public static Action onRespawn;


	public Button playButton;
	private int spawnTime =4;

	public override void Open ()
	{
		base.Open ();
		playButton.GetComponent<CanvasGroup>().alpha = 0.5f;
		playButton.interactable=false;
		txtReadyToPlay.gameObject.SetActive(false);
		txtTimer.gameObject.transform.parent.gameObject.SetActive(true);
		txtTimer.text = spawnTime.ToString();
	}

	protected override void OnOpenComplete ()
	{
		base.OnOpenComplete ();
		StartCoroutine(Timer ());
	}

	public override void Close ()
	{
		base.Close ();
	}

	public void OnPressPlay()
	{
		if(onRespawn!=null)
		{
			onRespawn();
		}
		UIController.ChangePanel(HTSequenceId.GamePlayScreen);
	}

	float t=0;
	IEnumerator Timer()
	{
		t = Time.time + 4f;
		while(t>=Time.time)
		{
			txtTimer.text = (  Mathf.Round(t-Time.time ) ).ToString();
			yield return null;
		}
		playButton.GetComponent<CanvasGroup>().alpha = 1f;
		playButton.interactable=true;
		txtReadyToPlay.gameObject.SetActive(true);
		txtTimer.gameObject.transform.parent.gameObject.SetActive(false);
	}
}
