﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class JoinScreen : UIPanel {
	public InputField txtNickInput;
	public void OnJoinClick()
	{
		Join();
	}

	bool isOpened = false;
	public override void Open ()
	{
		base.Open ();
		txtNickInput.Select ();
		txtNickInput.ActivateInputField ();

		isOpened = true;
	}

	public override void Close ()
	{
		base.Close ();
		isOpened = false;
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			Join ();
		}
	}

	private void Join()
	{
		if(!string.IsNullOrEmpty(txtNickInput.textComponent.text) && isOpened)
		{
			NetworkConnectionController.JoinRoom(txtNickInput.textComponent.text,()=>{ UIController.ChangePanel(HTSequenceId.GamePlayScreen);  });
			isOpened=false;
		}
	}
}
