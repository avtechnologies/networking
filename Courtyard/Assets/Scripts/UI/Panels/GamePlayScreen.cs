﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GamePlayScreen : UIPanel {

	public Text Score;
	public Text Rank;
	public Text teamAScore;
	public Text teamBScore;
	public NetworkConnectionController networkController;
	public Image mouseCursor;

	public VehicleTemperatureGauge gunTGauge;
	public VehicleTemperatureGauge engineTGauge;

	public static GamePlayScreen instance;
	public GameObject missileView;
	public Image[] missileIcons;


	protected override void Awake ()
	{
		base.Awake ();
		instance = this;
	}

	public override void Open ()
	{
		base.Open ();
		Cursor.visible = false;
	}

	public void ToggleLaserCannonView(bool isEnabled)
	{
		gunTGauge.GetComponent<CanvasGroup>().alpha = (isEnabled)?1f:0.3f;
	}

	public void ToggleMissileView(bool isEnabled)
	{
		missileView.GetComponent<CanvasGroup>().alpha = (isEnabled)?1f:0.3f;
	}

	public void UpdateMissileIcons(int amount)
	{
		foreach(Image m in missileIcons)
		{
			m.enabled = false;
		}

		for(int i=0;i<amount;i++)
		{
			missileIcons[i].enabled = true;
		}
	}
	
	private void Update()
	{
		mouseCursor.transform.position = Input.mousePosition;
		Score.text = "Score:" +PhotonNetwork.player.GetScore().ToString();

		List<PhotonPlayer> teamMembers = PunTeams.PlayersPerTeam[PhotonNetwork.player.GetTeam()];
		int rank =0;
		teamMembers.Sort((a,b)=>{return a.GetScore().CompareTo(b.GetScore());});
		for(int i=0;i<teamMembers.Count;i++)
		{
			if(PhotonNetwork.player == teamMembers[i])
			{
				rank = i;
			}
		}
		Rank.text = "Rank:"+rank.ToString();

		List<PhotonPlayer> teamA = PunTeams.PlayersPerTeam[PunTeams.Team.white];
		List<PhotonPlayer> teamB = PunTeams.PlayersPerTeam[PunTeams.Team.purple];

		int tAScore =0;
		int tBScore =0;
		foreach (var photonPlayer in teamA) {
			tAScore += photonPlayer.GetScore();
		}

		foreach (var photonPlayer in teamB) {
			tBScore += photonPlayer.GetScore();
		}

		teamAScore.text = "Team A:"+tAScore.ToString();
		teamBScore.text = "Team B:"+tBScore.ToString();
	}
	
	public override void Close ()
	{
		base.Close ();
		Cursor.visible = true;
	}
}
