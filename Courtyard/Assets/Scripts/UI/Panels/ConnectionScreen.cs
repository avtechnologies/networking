﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConnectionScreen : UIPanel {

	public Text txtStatus;
	
	public override void Open ()
	{
		base.Open ();
		PhotonNetwork.ConnectUsingSettings("v1.0");

		NetworkConnectionController.Connect("v1.0",()=>
			{
				txtStatus.text = "Joining Room...";
				UIController.ChangePanel(HTSequenceId.JoinScreen);
			});
	}
	
	void OnConnectedToPhoton()
	{
		Debug.Log("Connected to Photon");
	}
}
