﻿using UnityEngine;
using System.Collections;

public class NickTextPlaceholder : MonoBehaviour {

	public TextMesh textMesh;
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(Camera.main.transform.position);
	}
	
	public void SetText(string text)
	{
		textMesh.text = text;
	}
}
