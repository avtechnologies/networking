using UnityEngine;
using System.Collections;

// Require these components when using this script
[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof(PhotonView))]
public class BotControl : Photon.MonoBehaviour
{
	public Transform CameraPivot;
	public float animSpeed = 1.5f;				// a public setting for overall animator animation speed
	
	private Animator anim;							// a reference to the animator on the character
	private AnimatorStateInfo currentBaseState;			// a reference to the current state of the animator, used for base layer
	private AnimatorStateInfo layer2CurrentState;	// a reference to the current state of the animator, used for layer 2
	private CapsuleCollider col;					// a reference to the capsule collider of the character
	public NickTextPlaceholder nickTextPlaceHolder;

	private Vector3 latestCorrectPos;
	private Vector3 onUpdatePos;
	
	private Quaternion latestCorrectRot;
	private Quaternion onUpdateRot;
	
	private float fraction;

	void Awake()
	{
		latestCorrectPos = transform.position;
		onUpdatePos = transform.position;
		
		latestCorrectRot = transform.rotation;
		onUpdateRot = transform.rotation;
	}

	void Start ()
	{
		// initialising reference variables
		anim = GetComponent<Animator>();					  
		col = GetComponent<CapsuleCollider>();
		
		if(photonView.isMine)
		{
			Camera.main.transform.parent = CameraPivot;
			Camera.main.transform.localPosition = Vector3.zero;
			Camera.main.transform.localRotation = Quaternion.identity;
		}			
	}
	
	public string nick;
	
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			Vector3 pos = transform.position;
			Quaternion rot = transform.rotation;
			stream.Serialize(ref pos);
			stream.Serialize(ref rot);
		}
		else
		{
			// Receive latest state information
			Vector3 pos = Vector3.zero;
			Quaternion rot = Quaternion.identity;
			
			stream.Serialize(ref pos);
			stream.Serialize(ref rot);
			
			latestCorrectPos = pos;                 // save this to move towards it in FixedUpdate()
			latestCorrectRot = rot;
			onUpdatePos = transform.position;  // we interpolate from here to latestCorrectPos
			onUpdateRot = transform.rotation;
			fraction = 0;                           // reset the fraction we alreay moved. see Update()
			

		}
	}
	
	void OnPhotonInstantiate(PhotonMessageInfo info) { 
		nick = info.sender.name;
		if(photonView.isMine)
		{
			Destroy (nickTextPlaceHolder.gameObject);
		}
		else
		{
			nickTextPlaceHolder.SetText("<"+info.sender.name+">");
		}
	}
	
	
	void FixedUpdate ()
	{
		if(photonView.isMine )
		{
			if(Input.GetMouseButton(1))
			{
				transform.Rotate(0, Input.GetAxis("Mouse X") * 15f, 0);
			}
		
			float h = Input.GetAxis("Horizontal") ;				// setup h variable as our horizontal input axis
			float v = Input.GetAxis("Vertical") * ((Input.GetKey(KeyCode.LeftShift))?2f:1f);				// setup v variables as our vertical input axis
			anim.SetFloat("Speed", v);							// set our animator's float parameter 'Speed' equal to the vertical input axis				
			anim.SetFloat("Direction", h); 						// set our animator's float parameter 'Direction' equal to the horizontal input axis		
			anim.speed = animSpeed;
			return;
		}
									
		fraction = fraction + Time.deltaTime * 9;
		transform.position = Vector3.Lerp(onUpdatePos, latestCorrectPos, fraction);    // set our pos between A and B
		transform.rotation = Quaternion.Lerp(onUpdateRot, latestCorrectRot, fraction);    // set our pos between A and B														
																														// set the speed of our animator to the public variable 'animSpeed'
	}
}
