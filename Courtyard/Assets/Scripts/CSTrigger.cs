﻿using UnityEngine;
using System.Collections;

public class CSTrigger : MonoBehaviour {

	public CrossSectionTriggerGroup tGroup;
	public bool isOuter;
	// Use this for initialization

	void OnTriggerEnter(Collider other)
	{
		PlayerVehicle p = other.GetComponentInParent<PlayerVehicle>();
		if(p!=null && p.photonView.owner == PhotonNetwork.player)
		{
			tGroup.SetTriggerEnter(isOuter);
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		PlayerVehicle p = other.GetComponentInParent<PlayerVehicle>();
		if(p!=null && p.photonView.owner == PhotonNetwork.player)
		{
			tGroup.SetTriggerExit(isOuter);
		}
	}
}
