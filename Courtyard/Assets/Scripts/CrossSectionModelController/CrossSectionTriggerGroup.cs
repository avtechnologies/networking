﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;


public enum CSTriggerState
{
	None,
	EnteredOuter,
	EnteredInner,
	ExitOuter,
	ExitInner
}

public class CrossSectionTriggerGroup : MonoBehaviour {

	public CrossSectionModelController csController;
	private Vector3 positionOnEntry;
	[HideInInspector]
	public bool outerPoint = true;

	bool innerTriggerEntered= false;
	bool innerTriggerExit= false;
	
	bool outerTriggeredEntered = false;
	bool outerTriggerExit= false;
	public CSTriggerState state = CSTriggerState.None;

	public CSTriggerState[] stateSequence = new CSTriggerState[2];
	public int index =0;
	public bool isInside=false;
	

	public void SetTriggerEnter(bool isOuter)
	{
		stateSequence[index] = (isOuter)?CSTriggerState.EnteredOuter:CSTriggerState.EnteredInner;
		index++;
		if(index==2)
		{
			index =0;
		}
		CheckVisibility();
	}

	public void SetTriggerExit(bool isOuter)
	{
		stateSequence[index] = (isOuter)?CSTriggerState.ExitOuter:CSTriggerState.ExitInner;
		index++;
		if(index==2)
		{
			index =0;
		}
		CheckVisibility();
	}

	public void CheckVisibility()
	{
		if(stateSequence[0] == CSTriggerState.ExitOuter && stateSequence[1] == CSTriggerState.EnteredInner)
		{
			csController.HideSection();
		}

		if(stateSequence[0] == CSTriggerState.ExitInner && stateSequence[1] == CSTriggerState.ExitOuter)
		{
			csController.ShowSection();
		}

	}


	private void Reset()
	{
		stateSequence[0] = CSTriggerState.None;
		stateSequence[1] =  CSTriggerState.None;
	}
}
