﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class CrossSectionModelController : MonoBehaviour {

	public MeshRenderer topSectionRenderer;
	public Material opaqueMaterial;
	public Material transparentMaterial;
	private Color originalTintColor;
	[HideInInspector]
	public bool isInside = false;

	private Material topMaterial;
	private Material bottomMaterial;

	public MeshRenderer[] topRenderers;
	private Shader opaqueShader;
	private Shader transparentShader;
	
	

	Tweener alphaTween;

	void Awake()
	{
		originalTintColor = opaqueMaterial.GetColor("_TintColor");
		topMaterial = Instantiate(opaqueMaterial) as Material;
		bottomMaterial = Instantiate(opaqueMaterial) as Material;
		opaqueShader = opaqueMaterial.shader;
		transparentShader = transparentMaterial.shader;

		foreach(MeshRenderer mr in topRenderers)
		{
			mr.material = topMaterial;
		}
	}

	public void ShowSection()
	{
		ToggleTopVisibility(true);
		alphaTween = HOTween.To(this,1f,new TweenParms().Prop("Alpha",1,false).OnComplete(()=>{
			topMaterial.shader = opaqueShader;
		}));
	}

	private void ToggleTopVisibility(bool isEnabled)
	{
		foreach(MeshRenderer mr in topRenderers)
		{
			mr.enabled = isEnabled;
		}
	}

	public void HideSection()
	{
		topMaterial.shader = transparentShader;
		alphaTween = HOTween.To(this,1f,new TweenParms().Prop("Alpha",0.3f,false).OnComplete(()=>{
			//ToggleTopVisibility(false);
		}));
	}

	public float Alpha
	{
		set{
			topMaterial.SetColor("_TintColor",new Color(originalTintColor.r,originalTintColor.g,originalTintColor.b,value));
		}
		get{
			return topMaterial.GetColor("_TintColor").a;
		}
	}
	
}
