﻿using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour {

	public float health = 100f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	bool isDestroyed = false;

	public void ApplyDamage()
	{
		health -= 3f;
		if(health<=0 && !isDestroyed)
		{
			isDestroyed = true;
			Rigidbody[] rList = GetComponentsInChildren<Rigidbody>();
			foreach(Rigidbody r in rList)
			{
				r.transform.parent = null;
				r.isKinematic = false;
				r.AddExplosionForce(200f,transform.position,500f);
			}
		}
	}
}
