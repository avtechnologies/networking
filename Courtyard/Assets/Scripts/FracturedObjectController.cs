﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class FracturedObjectController : MonoBehaviour {

	public Rigidbody[] rigidbodies;
	public ParticleSystem[] fireps;
	public ParticleSystem explosion1;
	public Light explosionLight;
	[HideInInspector]
	public Vector3 direction;
	[HideInInspector]
	public float currentSpeed=0;
	
	// Use this for initialization
	void Awake () {
		rigidbodies = GetComponentsInChildren<Rigidbody>();
		fireps = GetComponentsInChildren<ParticleSystem>();
		explosionLight.enabled = false;
		explosion1.Play();
		
		foreach(ParticleSystem p in fireps)
		{
			p.Play();
		}
		
		foreach(Rigidbody r in rigidbodies)
		{
			r.transform.parent = null;
			r.isKinematic = false;
			r.AddExplosionForce(2000f,transform.position,200f);
			r.AddTorque(new Vector3(Random.Range(0,360),Random.Range(0,360),Random.Range(0,360))*1000f,ForceMode.Impulse);
		}

		explosionLight.enabled = true;
		HOTween.To(explosionLight,0.05f,new TweenParms().Prop ("intensity",1.67f,false).Ease(EaseType.EaseInQuad).OnComplete(()=>{HOTween.To(explosionLight,0.7f,new TweenParms().Prop ("intensity",0f,false).Ease(EaseType.EaseOutQuad).OnComplete(()=>{explosionLight.enabled=false;}));}));
		explosionLight.transform.LookAt (Camera.main.transform);
		Destroy(gameObject,5f);
	}

	void Update()
	{
		transform.position += direction.normalized * currentSpeed * Time.deltaTime;
		currentSpeed = Mathf.Lerp(currentSpeed,0,4f * Time.deltaTime);
	}
}
