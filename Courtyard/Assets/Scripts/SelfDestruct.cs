﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {

	// Use this for initialization
	void Awake () {

		ParticleSystem[] psList =  GetComponentsInChildren<ParticleSystem>();
		AudioSource[] asList =  GetComponentsInChildren<AudioSource>();

		float minTime = float.MaxValue;
		foreach(ParticleSystem p in psList)
		{
			minTime = Mathf.Min(p.startLifetime,minTime);
		}

		foreach(AudioSource a in asList)
		{
			minTime = Mathf.Min(a.clip.length,minTime);
		}

		if(minTime== float.MaxValue)
		{
			minTime = 4f;
		}
		Destroy (gameObject,minTime+1f);
	}

}
