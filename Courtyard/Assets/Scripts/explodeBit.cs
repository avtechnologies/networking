﻿using UnityEngine;
using System.Collections;

public class explodeBit : MonoBehaviour {


	AsteroidController asteroidController;

	void Awake()
	{
		asteroidController = GetComponentInParent<AsteroidController> ();
	}


	public void Explode()
	{
		asteroidController.ApplyDamage ();
	}
}
