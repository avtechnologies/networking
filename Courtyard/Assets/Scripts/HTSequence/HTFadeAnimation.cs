using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Core;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class HTFadeAnimation : HTControl
{
		private object _targetObject;
		private string _property = "alpha";
		public float targetAlpha;
	
		void Awake ()
		{
				_targetObject = GetTargetObject ();
		}
	
		object GetTargetObject ()
		{
				object targetObject = null;
		
				if ((targetObject = GetComponent<CanvasGroup> ()) != null) {
						return targetObject;
				} else {			
						Debug.LogError ("No compatible component types were found. Upgrade the HTFadeAnimation to handle other components");
						return null;
				}		
		}
	
	#region implemented abstract members of HTControl
		public override Holoville.HOTween.Tweener ToAnimation ()
		{
				//Added this code here so that if it is called pre-Awake, it won't generate a null reference exception
				if (_targetObject == null) {
						_targetObject = GetTargetObject ();
				}
		
				if (easeType == EaseType.AnimationCurve) {
						tween = HOTween.To (_targetObject, duration, new TweenParms ().Prop (_property, targetAlpha, false).Delay (delay).Ease (curve));
						return   tween;
				} else {
						tween = HOTween.To (_targetObject, duration, new TweenParms ().Prop (_property, targetAlpha, false).Delay (delay).Ease (easeType));
						return  tween;
				}	
		}

		public override Holoville.HOTween.Tweener FromAnimation ()
		{
				if (easeType == EaseType.AnimationCurve) {
						tween = HOTween.From (_targetObject, duration, new TweenParms ().Prop (_property, targetAlpha, false).Delay (delay).Ease (curve));
						return  tween;
				} else {
						tween = HOTween.From (_targetObject, duration, new TweenParms ().Prop (_property, targetAlpha, false).Delay (delay).Ease (easeType));
						return  tween;
				}	
		}
	#endregion
}
