using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Core;

public class HTScaleAnimation : HTControl
{
		public Vector3 scale;
	
	#region implemented abstract members of HTControl
		public override Holoville.HOTween.Tweener ToAnimation ()
		{
				tween = HOTween.To (transform, duration, new TweenParms ().Prop ("localScale", scale, false).Delay (delay).Ease (easeType));
				return   tween;
		}

		public override Holoville.HOTween.Tweener FromAnimation ()
		{
				tween = HOTween.From (transform, duration, new TweenParms ().Prop ("localScale", scale, false).Delay (delay).Ease (easeType));
				return   tween;
		}
	#endregion
}
