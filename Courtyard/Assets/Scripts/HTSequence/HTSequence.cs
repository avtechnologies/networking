using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;
using Holoville.HOTween.Core;
using System;

public enum HTSequenceId
{
	ConnectionScreen,
	JoinScreen,
	GamePlayScreen,
	SpawnScreen,
	TargetingScreen,
	GameSettings
}

public class HTSequenceEventArgs :EventArgs
{
		public bool isReverse;
		public HTSequenceId animationName;
}

public class HTSequence : MonoBehaviour
{
		public event HTSequenceCompleteHandler Complete;
		public event HTSequenceCompleteHandler Start;
	
		public delegate void HTSequenceCompleteHandler (HTSequenceEventArgs args);

		private Dictionary<HTSequenceId,Sequence> sequenceDictionary = new Dictionary<HTSequenceId, Sequence> ();
		private Dictionary<HTSequenceId,List<HTControl>> _sequenceControlsDictionary = new Dictionary<HTSequenceId, List<HTControl>> ();
		private static HTSequence instance;

		public static HTSequence Instance {
				get {
						if (instance == null) {
								instance = FindObjectOfType<HTSequence> ();
						}
						return instance;
				}
		}

		void Awake ()
		{
				if (instance != null && instance != this) {
						Destroy (instance.gameObject);
						return;
				}
				instance = this;
		}

		void SetActiveRecursively (GameObject go, bool active)
		{
				go.SetActive (active);
				foreach (Transform t in go.transform) {
						SetActiveRecursively (t.gameObject, active);
				}
		}
	
		private void OnComplete (HTSequenceEventArgs e)
		{
				if (Complete != null) {
						Complete (e);
				}
		}
	
		private void OnStart (HTSequenceEventArgs e)
		{
				if (Start != null) {
						Start (e);
				}
		}

		public void PopulateSequenceDictionary ()
		{
				List<HTControl> controls = new List<HTControl> ((FindObjectsOfType (typeof(HTControl)) as HTControl[]));
				foreach (HTControl g in controls) {
						if (!sequenceDictionary.ContainsKey (g.animationName)) {
								Sequence s = new Sequence (new SequenceParms ().UpdateType (g.updateType).AutoKill (false));
								Tweener t = g.GetTween ();
								List<HTControl> htControlList = new List<HTControl> ();
								htControlList.Add (g);
								if (g.easeType == EaseType.AnimationCurve) {
										t.easeAnimationCurve = g.curve;
								}
				
								s.Insert (g.delay, t);
								_sequenceControlsDictionary.Add (g.animationName, htControlList);
								sequenceDictionary.Add (g.animationName, s);
						} else {
								if (!_sequenceControlsDictionary [g.animationName].Contains (g)) {
										Tweener t = g.GetTween ();
										if (g.easeType == EaseType.AnimationCurve) {
												t.easeAnimationCurve = g.curve;
										}
										sequenceDictionary [g.animationName].Insert (g.delay, t);
										_sequenceControlsDictionary [g.animationName].Add (g);
								}
						}
				}
		}

		void OnDestroy ()
		{
				foreach (Sequence s in sequenceDictionary.Values) {
						s.Kill ();
				}
		}

		public void CompleteSequence (HTSequenceId sequenceName)
		{
				if (sequenceDictionary.ContainsKey (sequenceName)) {
						sequenceDictionary [sequenceName].Complete ();
				}
		}
	
		public void Play (HTSequenceId sequenceName)
		{
				Play (sequenceName, false, 1f, null);
		}
	
		public void Play (HTSequenceId sequenceName, bool reverse)
		{
				Play (sequenceName, reverse, 1f, null);
		}

		public void Play (HTSequenceId sequenceName, bool reverse, float timeScale)
		{
				Play (sequenceName, reverse, timeScale, null);
		}

		public void Play (HTSequenceId sequenceName, bool Reverse, float timeScale, TweenDelegate.TweenCallback onComplete)
		{
				if (!sequenceDictionary.ContainsKey (sequenceName)) {
						Debug.LogError ("You are trying to play a sequence that does not exist : " + sequenceName);
				}
				
				List<HTControl> htControls = _sequenceControlsDictionary [sequenceName];

				foreach (HTControl h in htControls) {
						h.UpdateEaseType (Reverse);
				}

				Sequence s = sequenceDictionary [sequenceName];

				s.timeScale = timeScale;
				if (!Reverse) {
						s.ApplyCallback (CallbackType.OnComplete, () => {
								OnComplete (new HTSequenceEventArgs (){ animationName = sequenceName, isReverse = Reverse});
								if (onComplete != null) {
										onComplete ();
								}
						});

						s.PlayForward ();
						OnStart (new HTSequenceEventArgs (){ animationName = sequenceName, isReverse = Reverse});
				} else {
						s.ApplyCallback (CallbackType.OnRewinded, () => {
								OnComplete (new HTSequenceEventArgs (){ animationName = sequenceName, isReverse = Reverse}); 
								if (onComplete != null) {
										onComplete ();
								}
						});
						s.PlayBackwards ();
						OnStart (new HTSequenceEventArgs (){ animationName = sequenceName, isReverse = Reverse});
				}
		}

		public void Remove (HTSequenceId sequenceId)
		{
				if (sequenceDictionary.ContainsKey (sequenceId)) {
						sequenceDictionary [sequenceId].Kill ();
						sequenceDictionary.Remove (sequenceId);
						_sequenceControlsDictionary [sequenceId].Clear ();
						_sequenceControlsDictionary.Remove (sequenceId);
				}
		}
}
