using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Core;

public enum HTAnimType
{
		From,
		To
}

public abstract class HTControl : MonoBehaviour
{
		public HTAnimType animationType;
		public EaseType easeType;
		public EaseType reverseEaseType;
		public float duration = 0.5f;
		public float delay = 0;
		public HTSequenceId animationName;
		public UpdateType updateType = UpdateType.Update;
		public Tweener tween;

		[HideInInspector]
		public AnimationCurve
				curve;
	
		public Tweener GetTween ()
		{
				if (animationType == HTAnimType.From) {
						return FromAnimation ();
				} else {
						return ToAnimation ();
				}
		}

		public void UpdateEaseType (bool isReversing)
		{
				if (tween != null) {
						tween.easeType = (isReversing) ? reverseEaseType : easeType;
				}
		}

		public abstract Tweener ToAnimation ();

		public abstract Tweener FromAnimation ();
}
