﻿using UnityEngine;
using System.Collections;

public class MissileController : Photon.MonoBehaviour {


	private float maxSpeed = 2000f;
	private float cSpeed =0;
	private const float ROTATION_SPEED = 6f;
	private const float LIFE_TIME = 6f;
	public GameObject explosionPrefab;

	private bool isLaunching = false;
	private PlayerVehicle sourceVehicle;
	private PlayerVehicle targetVehicle;
	private float lifeCounter =0;
	private float previousDistanceFromTarget;
	private PhotonPlayer missileTargetPhotonPlayer;
	public Collider missileCollider;
	private float missileArmTime =0.5f;
	private float surpassTime =0;
	private bool surpassed = false;
	private PhotonTransformView pTransformView;
	

	private void Awake ()
	{
		pTransformView = GetComponent<PhotonTransformView>();
		lifeCounter = float.MaxValue;
		lifeCounter = Time.time + LIFE_TIME;
		isLaunching = true;
		missileCollider.enabled = true;
		
		missileTargetPhotonPlayer = (PhotonPlayer)photonView.instantiationData[0];
		targetVehicle = NetworkConnectionController.GetVehicle(missileTargetPhotonPlayer);
		sourceVehicle = NetworkConnectionController.GetVehicle(photonView.owner);
		if(sourceVehicle!=null)
		{
			cSpeed = sourceVehicle.CurrentSpeed;
			Physics.IgnoreCollision(sourceVehicle.meshTransform.GetComponent<Collider>(),missileCollider);
			Physics.IgnoreCollision(sourceVehicle.GetComponent<CharacterController>(),missileCollider);
		}

		if(targetVehicle!=null)
		{
			previousDistanceFromTarget = Vector3.Distance(transform.position,targetVehicle.transform.position);
		}
	}
	
	private void Update () {
		if(photonView.isMine)
		{
			if(targetVehicle!=null)
			{
				if(!surpassed)
				{
					float currentDistanceFromTarget = Vector3.Distance(transform.position,targetVehicle.transform.position);
					if(currentDistanceFromTarget>previousDistanceFromTarget)
					{
						surpassed = true;
						surpassTime = Time.time+1.5f;
					}
					previousDistanceFromTarget = currentDistanceFromTarget;
					Vector3 dir = targetVehicle.transform.position - transform.position;
					Quaternion rot = Quaternion.LookRotation(dir);
					transform.rotation = Quaternion.Slerp (transform.rotation,rot, ROTATION_SPEED * Time.deltaTime);
				}
				else
				{
//					if(Time.time>surpassTime)
//					{
//						float currentDistanceFromTarget = Vector3.Distance(transform.position,targetVehicle.transform.position);
//						if(currentDistanceFromTarget<previousDistanceFromTarget)
//						{
//							surpassed = false;
//						}
//						previousDistanceFromTarget = currentDistanceFromTarget;
//						Vector3 dir = targetVehicle.transform.position - transform.position;
//						Quaternion rot = Quaternion.LookRotation(dir);
//						transform.rotation = Quaternion.Slerp (transform.rotation,rot, ROTATION_SPEED *  12f * Time.deltaTime);
//					}
				}

			}
			if(isLaunching)
			{
				Vector3 velocity = transform.forward * cSpeed;
				transform.position += (velocity * Time.deltaTime);
				pTransformView.SetSynchronizedValues(velocity,0);
				cSpeed = Mathf.Lerp(cSpeed,maxSpeed,Time.deltaTime/1f);
			}

			if(lifeCounter<Time.time)
			{
				photonView.RPC("DestroyMissile",PhotonTargets.AllBuffered);
			}
		}
	}

	public void OnPhotonPlayerDisconnected (PhotonPlayer otherPlayer)
	{

		if(photonView.owner == otherPlayer)
		{
			photonView.TransferOwnership(PhotonNetwork.masterClient);
		}
		Debug.Log("Disconnected : transfering ownership");
	}

	[PunRPC]
	public void DisableMotion()
	{
		isLaunching = false;
	}

	void OnTriggerEnter(Collider other)
	{
		if(photonView.isMine)
		{
			ShieldSphereController sController = other.GetComponent<ShieldSphereController>();
			if(sController!=null)
			{
				if(sController.photonView.owner == PhotonNetwork.player)
				{
					return;
				}
				photonView.RPC("DestroyMissile",PhotonTargets.AllBuffered);
			}

			CollisionDetector p = other.GetComponentInParent<CollisionDetector>();
			if(p!=null)
			{
				p.damageableObject.photonView.RPC ("ApplyDamage", PhotonTargets.All, 10000f, photonView.owner);
			}
			photonView.RPC("DestroyMissile",PhotonTargets.AllBuffered);
		}
	}

	[PunRPC]
	public void DestroyMissile()
	{
		Destroy(gameObject);
		isLaunching =false;
		Instantiate(explosionPrefab,transform.position,Quaternion.identity);
	}
}
