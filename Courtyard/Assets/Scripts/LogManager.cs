﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;

public class LogManager : MonoBehaviour {

	LogType logType;
	StringBuilder sb;
	public GUISkin customGUISkin;

	void OnEnable () {
		sb = new StringBuilder();
		sb.AppendLine("------------------");
		sb.AppendLine("System Info");
		sb.AppendLine("Device Model: " +SystemInfo.deviceModel);
		sb.AppendLine("Device Name: " + SystemInfo.deviceName);
		sb.AppendLine("DeviceType: " + SystemInfo.deviceType.ToString());
		sb.AppendLine("Graphics Device Name " + SystemInfo.graphicsDeviceName);
		sb.AppendLine("Graphics Memory Size: " + SystemInfo.graphicsMemorySize);
		sb.AppendLine("Operating System: " + SystemInfo.operatingSystem);
		sb.AppendLine("Graphics Shader Level: " + SystemInfo.graphicsShaderLevel);
		sb.AppendLine("processorType: " + SystemInfo.processorType);
		sb.AppendLine("systemMemorySizee: " + SystemInfo.systemMemorySize);
		Application.RegisterLogCallback(HandleLog);
	}
	void OnDisable () {
		// Remove callback when object goes out of scope
		Application.RegisterLogCallback(null);
	}
	Vector2 scrollPosition;
	void HandleLog (string logString ,string stackTrace, LogType type ) {
		logType = type;
		sb.AppendLine("");
		sb.AppendLine("Log Type: " + type.ToString());
		sb.AppendLine(logString);
		sb.AppendLine(stackTrace);
		sb.AppendLine("");
	}
	//&& (logType == LogType.Error || logType == LogType.Error || logType == LogType.Assert)
	bool logShown = false;
	void OnGUI()
	{
		GUI.skin = customGUISkin;
		if(GUILayout.Button(((!logShown)?"Show Log":"HideLog")))
		{
			logShown = !logShown;
		}

		if(!string.IsNullOrEmpty(sb.ToString())  && logShown)
		{
			if(GUILayout.Button("Send Error Report"))
			{
				StartCoroutine(SendEmailCoroutine());
			}
			scrollPosition = GUILayout.BeginScrollView(scrollPosition,GUILayout.Width(800),GUILayout.Height(500));
			GUILayout.BeginVertical("box");
			GUILayout.Label(sb.ToString());
			GUILayout.EndHorizontal();
			GUILayout.EndScrollView();
		}
	}

	IEnumerator SendEmailCoroutine()
	{
		WWWForm form = new WWWForm(); //here you create a new form connection
		form.AddField( "subject", "StarFighter Error Log " + DateTime.Now.ToShortDateString() + "  "  + DateTime.Now.ToShortTimeString() ); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
		form.AddField( "message", sb.ToString() );
		form.AddField( "email", "aoc1385@gmail.com" );
		WWW w = new WWW("http://game.av.com.mt/mailer/sendmail.php", form); //here we create a var called 'w' and we sync with our URL and the form
		yield return w; //we wait for the form to check the PHP file, so our game dont just hang
		if (w.error != null) {
			print(w.error); //if there is an error, tell us
		} else {
			print("Test ok");
			w.Dispose(); //clear our form in game
		}
	}
}
