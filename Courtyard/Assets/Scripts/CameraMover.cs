﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	private PlayerVehicle player;
	public AnimationCurve playerTrackingCurve;
	public float rate = 300f;
	private float currentD =0;
	// Use this for initialization

	private void Awake()
	{
		NotificationCenter.AddListener(HandleResetCamera,NotificationType.ResetCameraPosition);
	}

	private void OnDestroy()
	{
		NotificationCenter.RemoveListener(HandleResetCamera,NotificationType.ResetCameraPosition);
	}

	void HandleResetCamera (Notification note)
	{
		PlayerVehicle player = (PlayerVehicle)note.data;
		if(player.photonView.owner == PhotonNetwork.player)
		{
			transform.position = Vector3.Scale(player.transform.position, new Vector3(1f,0,1f)) + new Vector3(0,transform.position.y,0);
		}
	}

	// Update is called once per frame
	private void LateUpdate () {
		if (player == null) {
			PlayerVehicle[] players = FindObjectsOfType<PlayerVehicle> ();
			foreach (PlayerVehicle p in players) {
				if (p.photonView.isMine) {
					player = p;
					break;
				}
			}
		} else {
			float playerTrackPercentage = playerTrackingCurve.Evaluate(player.viewPortDistanceFromCenter);
			Vector3 targetPosition = Vector3.Scale(player.transform.position, new Vector3(1f,0,1f)) + new Vector3(0,transform.position.y,0) + player.transform.forward * playerTrackPercentage * 600f;
			float d =  1f/Mathf.Max ( Vector3.Distance(targetPosition,transform.position)/rate,1f);
			currentD = Mathf.Lerp(currentD,d,Time.deltaTime/6f);
			transform.position = Vector3.Lerp(transform.position,targetPosition, playerTrackPercentage * 4f * Time.deltaTime);
		}
	}
}
