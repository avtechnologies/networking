﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System;
using System.Net;
using System.ComponentModel;

public class SaveProjectAndSceneEditor : EditorWindow {

	// Add menu named "My Window" to the Window menu
	[MenuItem ("Window/Saver")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		SaveProjectAndSceneEditor window = (SaveProjectAndSceneEditor)EditorWindow.GetWindow (typeof (SaveProjectAndSceneEditor));
		window.LoadSettings();
		window.Show();
	}

	private float progress =0;
	string previousVersion = "";

	void OnEnabled()
	{
		LoadSettings();
		previousVersion = settings.version.ToString();
	}

	string progressTxt ="";
	string buildName;
	string folderPath;
	string buildPath;

	void OnGUI () {
		buildName = settings.buildNamePrefix + "v" + settings.version.ToString().Replace('.','_');
		folderPath = settings.buildFolder +"/";
		buildPath = folderPath + buildName ;
		
		if(GUILayout.Button("Save Scene and Assets"))
		{
			EditorApplication.SaveScene();
			EditorApplication.SaveAssets();
			UnityEngine.Debug.LogFormat(string.Format("<color=green>Scene and Assets Saved ({0})</color>",System.DateTime.Now.ToShortTimeString()));
		}

		GUILayout.Label("BuildTarget: " + EditorUserBuildSettings.activeBuildTarget);
		GUILayout.Label("Build Location: " + settings.buildFolder);
		GUILayout.Space(10f);

		GUILayout.BeginHorizontal();
			GUILayout.Label("Current Version: ");
			previousVersion = GUILayout.TextField(previousVersion);
			if(settings.version.ToString()!=previousVersion)
			{
				float result =0;
				if(float.TryParse(previousVersion,out result))
				{
					settings.version = result;
					EditorUtility.SetDirty(settings);
				}
			}
		GUILayout.EndHorizontal();

		if(GUILayout.Button ("Build"))
		{

			BuildGame();
		}
		GUI.enabled = (File.Exists(buildPath + "/" + buildName + ".unity3d"));
		if(GUILayout.Button ("Upload"))
		{
			if(bWorker!=null && bWorker.IsBusy)
			{
				return;
			}
//			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows64);
			progress =0;

			progressTxt = "Starting Upload";
			Upload(buildPath + "/index.html",()=>{
				EditorApplication.update += HandleCallbackFunction;
				Upload(buildPath + "/" + buildName + ".unity3d" ,()=>{
					EditorApplication.update -= HandleCallbackFunction;
				});
			});
		}
		Rect nextRect = GUILayoutUtility.GetRect(40f,15f);
		EditorGUI.ProgressBar(nextRect,progress,progressTxt);
		GUI.enabled = true;
	}

	private float previousProgress=0;
	void HandleCallbackFunction ()
	{
		if(progress>=0.99f && progressTxt != "Starting Upload")
		{
			progressTxt = "Upload Complete";
			Repaint();
		}
		if(progress!=previousProgress)
		{
			progressTxt = string.Format("{0}%",progress * 100f);
			Repaint();
		}
		previousProgress = progress;
	}
	
	public void BuildGame ()
	{

		List<string> sceneList = new List<string>();
		foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
		{
			if(S.enabled)
			{
				sceneList.Add(S.path);
			}
		}

		if(Directory.Exists(buildPath))
		{
			Directory.Delete(buildPath,true);
		}

		BuildPipeline.BuildPlayer(sceneList.ToArray(), buildPath, BuildTarget.WebPlayerStreamed, BuildOptions.Development);
		File.Move(buildPath + "/" + buildName + ".html",buildPath + "/index.html");
		string indexFile = File.ReadAllText(buildPath + "/index.html");
		indexFile = indexFile.Replace("%VERSION%",settings.version.ToString());
		File.WriteAllText(buildPath + "/index.html",indexFile,Encoding.ASCII);
	}

	private AutoWebBuildSettings settings;
	private void LoadSettings()
	{
		settings = Resources.Load<AutoWebBuildSettings>("AutoWebBuildSettings");
		previousVersion = settings.version.ToString();
	}


	BackgroundWorker bWorker;
	private Action onCompleteUpload;

	public void Upload(string filePath, Action OnComplete)
	{
		onCompleteUpload = OnComplete;
		bWorker = new BackgroundWorker();
		bWorker.WorkerReportsProgress = true;

		bWorker.RunWorkerCompleted += HandleRunWorkerCompleted;
		bWorker.ProgressChanged += HandleProgressChanged;
		bWorker.DoWork += HandleUploadDoWork;
		
		bWorker.RunWorkerAsync(filePath);
	}

	void HandleProgressChanged (object sender, ProgressChangedEventArgs e)
	{
		progress = (float)e.ProgressPercentage/100f;
	}

	void HandleRunWorkerCompleted (object sender, RunWorkerCompletedEventArgs e)
	{
		if(e.Error!=null)
		{
			Debug.Log(e.Error.Message);
		}
		bWorker.Dispose();
		bWorker = null;

		if(onCompleteUpload!=null)
		{
			onCompleteUpload();
		}
	}

	private void HandleUploadDoWork(object sender, DoWorkEventArgs e)
	{
		string filePath = (string)e.Argument;
		string[] temp = filePath.Split('/');
		string fileName = temp[temp.Length-1];
		var ftpWebRequest = (FtpWebRequest)WebRequest.Create(settings.ftp + fileName);
		ftpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;
		ftpWebRequest.Credentials = new NetworkCredential (settings.username,settings.password);
		var inputStream = File.OpenRead(filePath);
		var outputStream = ftpWebRequest.GetRequestStream();

		var buffer = new byte[1024 * 16];
		int totalReadBytesCount = 0;
		int readBytesCount;
		while ((readBytesCount = inputStream.Read(buffer, 0, buffer.Length)) > 0)
		{
			outputStream.Write(buffer, 0, readBytesCount);
			totalReadBytesCount += readBytesCount;
			var progress = totalReadBytesCount * 100.0 / inputStream.Length;
			bWorker.ReportProgress((int)progress);
		}

		inputStream.Flush();
		inputStream.Close();
		outputStream.Flush();
		outputStream.Close();
		inputStream = null;
		outputStream = null;
	}
}