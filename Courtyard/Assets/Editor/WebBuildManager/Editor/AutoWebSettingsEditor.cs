﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AutoWebBuildSettings))]
public class AutoWebSettingsEditor : Editor {

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		AutoWebBuildSettings settings = (AutoWebBuildSettings)target;
		if(GUILayout.Button("Select Build Folder"))
		{
			settings.buildFolder = EditorUtility.SaveFolderPanel("Choose Location for builds", "", "");
			EditorUtility.SetDirty(settings);
		}
	}
}
