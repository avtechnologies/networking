﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AssetBundleExporter
{
	[MenuItem ("Assets/Build AssetBundles")]
	static void BuildAllAssetBundles ()
	{
		BuildPipeline.BuildAssetBundles ("Assets/AssetBundles",BuildAssetBundleOptions.CollectDependencies,BuildTarget.WebPlayer);
	}
}
