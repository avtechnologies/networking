﻿
Shader "QuantumTheory/VertexColors/Unity5/Diffuse Rim Transparent"{
	Properties {
		_TintColor ("Rim Color", Color) = (1,1,1,0.0)
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      	_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	}
	
	SubShader {
          Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
          Blend SrcAlpha OneMinusSrcAlpha
          LOD 200
          Cull Back
          ZWrite On
		
		// Surface Shader Passes:
		CGPROGRAM
		#pragma surface surf Lambert alpha:fade
		#pragma target 3.0
		#pragma glsl
		#include "QT_Cubemaps.cginc"
		
		struct Input {
			float4 color : COLOR;
			float3 worldNormal;
			float3 viewDir;
		};
		
		float4 _TintColor;
		float4 _RimColor;
      	float _RimPower;
		
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.color.rgb * _TintColor.rgb;
			o.Alpha = _TintColor.a;
			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          	o.Emission = _RimColor.rgb * pow (rim, _RimPower);
		}
		ENDCG
		
		// Vertex Lit:
		Pass {
			Tags { "LightMode" = "Vertex" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			
			
			ENDCG
		}
		// Vertex Lit Lightmapped:
		Pass {
			Tags { "LightMode" = "VertexLM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
		Pass {
			Tags { "LightMode" = "VertexLMRGBM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
	}
	Fallback "QuantumTheory/VertexColors/VertexLit"	
}