﻿
Shader "QuantumTheory/VertexColors/Unity5/Diffuse Transparent"{
	Properties {
		_TintColor ("Tint Color", Color) = (1,1,1,0.0)
	}
	
	SubShader {
          Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
          Blend SrcAlpha OneMinusSrcAlpha
          LOD 200
          Cull Back
          ZWrite On
		
		// Surface Shader Passes:
		CGPROGRAM
		#pragma surface surf Lambert alpha:fade
		#pragma target 3.0
		#pragma glsl
		#include "QT_Cubemaps.cginc"
		
		struct Input {
			float4 color : COLOR;
			float3 worldNormal;
		};
		
		float4 _TintColor;
		
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.color.rgb * _TintColor.rgb;
			o.Alpha = _TintColor.a;
		}
		ENDCG
		
		// Vertex Lit:
		Pass {
			Tags { "LightMode" = "Vertex" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			
			
			ENDCG
		}
		// Vertex Lit Lightmapped:
		Pass {
			Tags { "LightMode" = "VertexLM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
		Pass {
			Tags { "LightMode" = "VertexLMRGBM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
	}
	Fallback "QuantumTheory/VertexColors/VertexLit"	
}