﻿// Copyright © 2014 Laurens Mathot
// Code Animo™ http://codeanimo.com
// License terms can be found at the bottom of this file.

Shader "QuantumTheory/VertexColors/Unity5/Diffuse Rim" {
	Properties {
		_TintColor ("Rim Color", Color) = (1,1,1,0.0)
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      	_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 300
		
		// Surface Shader Passes:
		CGPROGRAM
		#pragma surface surf Lambert addshadow
		#pragma target 3.0
		#pragma glsl
		#include "QT_Cubemaps.cginc"
		
		struct Input {
			float4 color : COLOR;
			float3 worldNormal;
			float3 viewDir;
		};
		
		float4 _TintColor;
		float4 _RimColor;
      	float _RimPower;
	
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.color.rgb * _TintColor.rgb;
			half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          	o.Emission = _RimColor.rgb * pow (rim, _RimPower);
		}
		ENDCG
		
		// Vertex Lit:
		Pass {
			Tags { "LightMode" = "Vertex" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			
			
			ENDCG
		}
		// Vertex Lit Lightmapped:
		Pass {
			Tags { "LightMode" = "VertexLM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
		Pass {
			Tags { "LightMode" = "VertexLMRGBM" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma glsl
			
			#include "UnityCG.cginc"
			#include "VertexLitIBL.cginc"
			
			ENDCG
		}
	}
	Fallback "QuantumTheory/VertexColors/VertexLit"
	
	
}

// Copyright © 2014 Laurens Mathot
// Code Animo™ http://codeanimo.com
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.