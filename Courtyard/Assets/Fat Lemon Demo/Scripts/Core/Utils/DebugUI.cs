﻿using UnityEngine;
using System.Collections;

public delegate void DebugUIDelegate ();

public class DebugUI : MonoBehaviour
{
	private const int BUTTON_WIDTH = 185;
	private const int BUTTON_HEIGHT = 35;

	public static DebugUIDelegate debugView;
	
	public static GUILayoutOption ButtonWidth {
		get {
			return GUILayout.Width (BUTTON_WIDTH);
		}
	}
	
	public static GUILayoutOption ButtonHeight {
		get {
			return GUILayout.Height (BUTTON_HEIGHT);
		}
	}
	    	
	private static DebugUI  instance;

	private static DebugUI  Instance {
		get {
			if (instance == null) {
				instance = FindObjectOfType (typeof(DebugUI)) as DebugUI;
			}
			return instance;
		}
	}

	private bool showGUI;
	private AudioListener audioListener;
	
	void Awake ()
	{
		instance = this;
		audioListener = GetComponent<AudioListener> ();
	}
	
	#if DEBUG
	void OnGUI ()
	{
		if (GUILayout.Button ("Show GUI", DebugUI.ButtonWidth, DebugUI.ButtonHeight)) {
			showGUI = !showGUI;
		}
		
		if (showGUI) {
			if (GUILayout.Button ("Get Date", DebugUI.ButtonWidth, DebugUI.ButtonHeight)) {
				AVDebug.Log (System.DateTime.Now.ToLongDateString ());
			}

			if (GUILayout.Button ("Clear PlayerPrefs", DebugUI.ButtonWidth, DebugUI.ButtonHeight)) {
				PlayerPrefs.DeleteAll ();
			}

			if (GUILayout.Button (audioListener.enabled ? "Mute" : "Unmute", DebugUI.ButtonWidth, DebugUI.ButtonHeight)) {
				audioListener.enabled = !audioListener.enabled;
			}

			if (debugView != null) {
				debugView ();
			}
		}
	}
	#endif
}
