using UnityEngine;
using System.Collections;

public class AssertionFailedException : UnityException
{
	public AssertionFailedException(string message) : base(message)
	{

	}
}


/// <summary>
/// This class is a wrapper for Debug.Log to easily strip out any logs in the final release versions.
///
/// All the logs (.Log, .LogWarning, .LogError) will be output in the console if:
/// - it is played inside the editor,
/// - or DEBUG scripting define symbol is used,
/// - or LOG_ALL scripting define symbol is used
///
/// We can filter out the .Logs and keep Warnings and Errors by using the LOG_WARN_ERROR scripting define symbol
/// We can keep only the Error logs by using the LOG_ERROR_ONLY scripting define symbol
///
/// E.g. This gives us the flexibility to create a release with Warning and Errors by just using the LOG_WARN_ERROR
/// scripting define symbol
/// </summary>
public class AVDebug
{
	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void Log(object msg)
	{
		Debug.Log(msg);
	}

	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void Log(object msg, Object context)
	{
		Debug.Log(msg, context);
	}

	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("LOG_WARN_ERROR"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void LogWarning(object msg)
	{
		Debug.LogWarning(msg);
	}

	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("LOG_WARN_ERROR"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void LogWarning(object msg, Object context)
	{
		Debug.LogWarning(msg, context);
	}

	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("LOG_WARN_ERROR"),
	 System.Diagnostics.Conditional("LOG_ERROR_ONLY"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void LogError(object msg)
	{
		Debug.LogError(msg);
	}

	[System.Diagnostics.Conditional("DEBUG"),
	 System.Diagnostics.Conditional("LOG_ALL"),
	 System.Diagnostics.Conditional("LOG_WARN_ERROR"),
	 System.Diagnostics.Conditional("LOG_ERROR_ONLY"),
	 System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void LogError(object msg, Object context)
	{
		Debug.LogError(msg);
	}

	[System.Diagnostics.Conditional("DEBUG")]
	public static void Assert(bool condition, string message)
	{
		if (!condition)
		{
			Debug.Break();
			throw new AssertionFailedException(message);
		}
	}
}
