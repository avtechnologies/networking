using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using System.Reflection;
using System.Text;

#endif

//Based off http://forum.unity3d.com/threads/37896-Programming-architectures?p=847646&viewfull=1#post847646
//Changed it to static methods (less typing and less indirection)
//Added helper methods for posting notifications

public delegate void OnNotificationDelegate (Notification note);

public class NotificationCenter
{
	private static OnNotificationDelegate[] listeners = new OnNotificationDelegate[Enum.GetValues (typeof(NotificationType)).Length];

	/// <summary>
	/// Adds the listener. Delegate signature: oid OnNotificationDelegate(Notification note) 
	/// </summary>
	public static void AddListener (OnNotificationDelegate listenerDelegate, NotificationType type)
	{
		int typeInt = (int)type;
		//Note that these checks will only be done when DEBUG is defined
		//DebugUtils.Assert(!IsAlreadyRegistered(listenerDelegate, typeInt), "You have already registered this delegate for " + type);
		listeners [typeInt] += listenerDelegate;
	}

	public static void RemoveListener (OnNotificationDelegate listenerDelegate, NotificationType type)
	{
		int typeInt = (int)type;
		listeners [typeInt] -= listenerDelegate;
	}

	private static bool IsAlreadyRegistered (OnNotificationDelegate newListenerDelegate, int typeInt)
	{
		if (listeners [typeInt] == null) {
			return false;
		}

		Delegate[] invocationList = listeners [typeInt].GetInvocationList ();
		foreach (Delegate d in invocationList) {
			if (d == newListenerDelegate) {
				return true;
			}
		}
		return false;
	}
	
	#if UNITY_EDITOR
	public static void PrintInvocationList ()
	{
		StringBuilder sb = new StringBuilder ();
		sb.AppendLine ("NotificationCenter Listeners");
		sb.AppendLine ("----------------------------");

		for (int i = 0; i < listeners.Length; i++) {
			if (listeners [i] == null) {
				continue;
			}

			Delegate[] invocationList = listeners [i].GetInvocationList ();
			foreach (Delegate d in invocationList) {
				sb.AppendLine (string.Format ("\tType - {0}, Method - {1}", d.Method.DeclaringType.FullName, d.Method.Name));
			}
		}

		AVDebug.Log (sb.ToString ());
	}
	#endif

	public static void Post (NotificationType type)
	{
		Post (new Notification (type));
	}

	public static void Post (NotificationType type, object dataObject)
	{
		//AVDebug.LogVerbose(string.Format("<color=green>Posting {0}</color>", type));
		Post (new Notification (type, dataObject));

	}
	
	public static void Reset_USE_ONLY_FOR_UNIT_TESTS ()
	{
		listeners = new OnNotificationDelegate[Enum.GetValues (typeof(NotificationType)).Length];
	}
	
	public static void Post (Notification notification)
	{
		int typeInt = (int)notification.type;
		if (listeners [typeInt] != null) {
			listeners [typeInt] (notification);
		}
	}
}
