﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using JsonFx;
using JsonFx.Json;

public partial class Parameters
{

	private static float minimum_rotation_mouse_distance = 120f;
	public static float MINIMUM_ROTATION_MOUSE_DISTANCE{
		get { return minimum_rotation_mouse_distance; }
		internal set { minimum_rotation_mouse_distance = value; }
	}

	private static float min_speed=200f;
	public static float MIN_SPEED{
		get { return min_speed; }
		internal set { min_speed = value; }
	}

	private static float max_speed=600f;
	public static float MAX_SPEED{
		get { return max_speed; }
		internal set { max_speed = value; }
	}

	private static float nitro_speed=1400f;
	public static float NITRO_SPEED{
		get { return nitro_speed; }
		internal set { nitro_speed = value; }
	}

	private static float rotation_speed = 15f;
	public static float ROTATION_SPEED{
		get { return rotation_speed; }
		internal set { rotation_speed = value; }
	}

	private static float accelerator_mouse_distance= 350f;
	public static float ACCELERATOR_MOUSE_DISTANCE{
		get { return accelerator_mouse_distance; }
		internal set { accelerator_mouse_distance = value; }
	}

	private static float engine_max_heat = 3f;
	public static float ENGINE_MAX_HEAT{
		get { return engine_max_heat; }
		internal set { engine_max_heat = value; }
	}

	private static float max_bank_speed = 40f;
	public static float MAX_BANK_SPEED{
		get { return max_bank_speed; }
		internal set { max_bank_speed = value; }
	}

	private static float max_health =100f;
	public static float MAX_HEALTH{
		get { return max_health; }
		internal set { max_health = value; }
	}
}

public class FatLemonManager : MonoBehaviour
{
	private static string SERVER_URL = "http://game.av.com.mt/FatLemon/src/";

	public Parameters parameters;
	public Action onParamsChanged;

	private static FatLemonManager instance;
	public static FatLemonManager Instance {
		get {
			if (instance == null) {
				instance = FindObjectOfType (typeof(FatLemonManager)) as FatLemonManager;
				if (instance == null) {
					Debug.LogError ("No instance of type FatLemonManager was found. Make sure you have one instance of type FatLemonManager in the scene.");
				}
			}
			return instance;
		}
	}

	void Awake ()
	{
		if (instance != null && instance != this) {
			Debug.LogError ("An instance of FatLemonManager already exists. Current instance will be destroyed.");
			Destroy (gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad (this);
	}

	void Start ()
	{
		parameters = new Parameters ();
		onParamsChanged += () => {
			Debug.Log (parameters.ToString ());
		};
		Sync ();
	}

	void OnEnable ()
	{
		DebugUI.debugView += OnDebugView;
	}

	void OnDisable ()
	{
		DebugUI.debugView -= OnDebugView;
	}

	public void Sync ()
	{
		StartCoroutine (SubmitParametersCoroutine (() => {
			StartCoroutine (RetrieveParametersCoroutine (() => {
				if (onParamsChanged != null) {
					onParamsChanged ();
				}}));
		}));
	}

	private IEnumerator SubmitParametersCoroutine (Action onCompleted)
	{
		var data = JsonWriter.Serialize (parameters);
		Debug.Log ("Submitting " + data);

		WWWForm wwwForm = new WWWForm ();
		wwwForm.AddField ("data", data);
		
		WWW www = new WWW (SERVER_URL + "MergeParameters.php", wwwForm);
		yield return www;
		
		if (www.error != null) {
			Debug.LogError (www.error);
		} else {
			onCompleted ();
		}
	}

	private IEnumerator RetrieveParametersCoroutine (Action onCompleted)
	{
		WWW www = new WWW (SERVER_URL + "RetrieveParameters.php");
		yield return www;

		if (www.error != null) {
			Debug.LogError (www.error);
		} else {
			parameters = JsonReader.Deserialize<Parameters> (www.text);
			print(www.text);
			onCompleted ();
		}
	}

	private void OnDebugView ()
	{
		if (GUILayout.Button ("Sync Parameters", DebugUI.ButtonWidth, DebugUI.ButtonHeight)) {

		}
	}
}
