﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonoExtensions;

public class PhysicsCamera : MonoBehaviour
{
		
	public PlayerForceController player;
	public AnimationCurve playerTrackingCurve;
	public float velocityMagnitude = 0;
	public float normalMotionViewRadius = 30f;
	public float fastMotionViewRadius = 30f;
	private float deltaFocus =0;
	private float lerpedDeltaFocus =1f;
	private Vector3 velocity = Vector3.zero;
	private float fastSmoothTime = 0.030f;
	private float slowSmoothTime = 0.3f;
	public float smoothTime;
	public Vector3 focusVelocity;
	
	Vector3 mainFocusPoint;
	Vector3 externalFocusPoint;
	Vector3 netFocusPoint;
	float entityFocusAttractionDistance = 150f;
	public AnimationCurve entityFocusAttractionCurve;
	float fe =1f;

	void Awake ()
	{
	}
		
	// Use this for initialization

	// Update is called once per frame
	private void FixedUpdate ()
	{
		float dt = Time.fixedDeltaTime;
		velocityMagnitude = player.rigidbody.velocity.magnitude;
		Vector3 camPosOnPlayerPlane = transform.position.ReplaceComponent (VectorComponent.Z, player.transform.position.z);
		//					 safeBoxCenter = transform.position;
		float playerTrackPercentage = playerTrackingCurve.Evaluate (player.viewPortDistanceFromCenter);

//		ScriptExtensions.DrawCross (safeBoxCenter, 2f, Color.black);xw
		ScriptExtensions.DrawCircle (camPosOnPlayerPlane, normalMotionViewRadius, Color.blue);
		ScriptExtensions.DrawCircle (camPosOnPlayerPlane, fastMotionViewRadius, Color.green);
		mainFocusPoint =  player.transform.position + (player.transform.up * (normalMotionViewRadius * playerTrackPercentage));
//		CameraFocusAttractor c = GetFocusAttractor(player.transform.position,entityFocusAttractionDistance);
//		if(c!=null)
//		{
//			fe = entityFocusAttractionCurve.Evaluate(Mathf.Min (Vector3.Distance(player.transform.position,c.transform.position),entityFocusAttractionDistance)/entityFocusAttractionDistance);
//			externalFocusPoint = Vector3.Lerp(player.transform.position,c.transform.position,0.8f);
//			ScriptExtensions.DrawCross (externalFocusPoint, 4f, Color.grey);
//			netFocusPoint = Vector3.Lerp(externalFocusPoint,mainFocusPoint,fe);
//		}
//		else
		{
			netFocusPoint = mainFocusPoint;
		}


		deltaFocus = Vector2.Dot(player.transform.up,player.mouseDirection)/2f +0.5f; 
		lerpedDeltaFocus = Mathf.Min(lerpedDeltaFocus,deltaFocus);
		lerpedDeltaFocus = Mathf.Lerp(lerpedDeltaFocus,deltaFocus,0.8f*dt);
		smoothTime = Mathf.Lerp(slowSmoothTime,fastSmoothTime,lerpedDeltaFocus);

		
		ScriptExtensions.DrawCross (netFocusPoint, 2f, Color.black);

		if(player.engineForceMultiplier * player.thrusterForceMultiplier==0)
		{
			float d = Vector3.Distance (player.transform.position, camPosOnPlayerPlane);
			if (d > fastMotionViewRadius) 
			{
				Vector3 dir = (player.transform.position - camPosOnPlayerPlane).normalized;
				float diff = d - fastMotionViewRadius;
				Vector3 newTarget = camPosOnPlayerPlane + (dir * diff);
				ScriptExtensions.DrawCross (newTarget, 2f, Color.yellow);
				transform.position = Vector3.SmoothDamp(transform.position, newTarget.ReplaceComponent(VectorComponent.Z,transform.position.z), ref velocity, slowSmoothTime);
			}
		}
		else
		{
			transform.position = Vector3.SmoothDamp(transform.position, netFocusPoint.ReplaceComponent(VectorComponent.Z,transform.position.z), ref velocity, smoothTime + (1f-player.engineForceMultiplier)  + (1f-player.thrusterForceMultiplier));
		}
	}

	private CameraFocusAttractor GetFocusAttractor(Vector3 mainAttractorPosition, float minimumDistance)
	{
		CameraFocusAttractor[] attractors = FindObjectsOfType<CameraFocusAttractor>();
		List<CameraFocusAttractor> candidates = new List<CameraFocusAttractor>();
		foreach (var cameraFocusAttractor in attractors) {
			float distance = Vector3.Distance(mainAttractorPosition,cameraFocusAttractor.transform.position);
			if(distance<minimumDistance)
			{
//				Vector3 dir = mainAttractorPosition-cameraFocusAttractor.transform.position;
//				float dot =Vector3.Dot(player.transform.forward,dir);
//				float theta = Mathf.Rad2Deg * Mathf.Acos(dot/ (dir.magnitude * player.transform.forward.magnitude));
//				return (dot>=0 && theta<MINIMUM_LOCK_ANGLE);
				cameraFocusAttractor.distance = distance;
				candidates.Add(cameraFocusAttractor);
			}
		}
		if(candidates.Count>0)
		{
			candidates.Sort((a,b)=>{ return a.distance.CompareTo(b.distance);});
			return candidates[0];
		}
		return null;
	}

}
