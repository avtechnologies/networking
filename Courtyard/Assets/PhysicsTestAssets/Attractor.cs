﻿using UnityEngine;
using System.Collections;

public class Attractor : MonoBehaviour {

	public float radiusOfInfluence = 30f;
	public AnimationCurve fallOff;
	public float attractionForce = 20f;

	// Update is called once per frame
	private void FixedUpdate () {
		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position,radiusOfInfluence);
		foreach(Collider2D c in colliders)
		{
			Rigidbody2D r = c.GetComponent<Rigidbody2D>();
			if(r)
			{
				Vector3 dir = (transform.position - c.transform.position).normalized;
				float distance = Vector3.Distance(transform.position,c.transform.position);
				float percentageDistance = distance/radiusOfInfluence;
				float f = fallOff.Evaluate(percentageDistance);
				r.AddForce(dir * f * attractionForce,ForceMode2D.Force);
				Debug.DrawLine (transform.position,c.transform.position, new Color(f,1f-f,1f-f));

			}
		}
	}

	void OnDrawGizmos()
	{
		Vector3 position = transform.position;
		Vector3 previousPt =  new Vector3(position.x + radiusOfInfluence * Mathf.Cos (0),position.y + radiusOfInfluence * Mathf.Sin (0),position.z);
		float step = 360f/24f;
		for(float i=step;i<=360f;i+= step )
		{
			Vector3 nextPt = new Vector3(position.x + radiusOfInfluence * Mathf.Cos (Mathf.Deg2Rad *i),position.y + radiusOfInfluence * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
			Gizmos.color = Color.red;
			Gizmos.DrawLine(previousPt,nextPt);
			previousPt = nextPt;
		}
	}
}
