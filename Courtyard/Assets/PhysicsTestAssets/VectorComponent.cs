﻿using UnityEngine;
using System.Collections;

namespace MonoExtensions
{
	public static class ScriptExtensions
	{
		public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius,float torquePush)
		{
			var dir = (body.transform.position - explosionPosition);
			float wearoff = 1 - (Mathf.Min(dir.magnitude,explosionRadius) / explosionRadius);
			body.AddForce(dir.normalized * explosionForce * wearoff);
//			body.AddTorque(Random.Range(-torquePush,torquePush),ForceMode2D.Impulse);
		}

		public static void DrawCircle(Vector3 position, float radius, Color color, int points = 24)
		{
			Vector3 previousPt =  new Vector3(position.x + radius * Mathf.Cos (0),position.y + radius * Mathf.Sin (0),position.z);
			float step = 360f/24f;
			for(float i=step;i<=360f;i+= step )
			{
				Vector3 nextPt = new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad * i),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
				Debug.DrawLine(previousPt,nextPt,color);
				previousPt = nextPt;
			}
		}
		
		public static void DrawBox(Vector3 position, float size, Color color, bool drawCross = false)
		{
			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),color);
			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),color);
			Debug.DrawLine(new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
			if(drawCross)
			{
				Debug.DrawLine(new Vector3(position.x-(size/2f),position.y + (size/2f),position.z),new Vector3(position.x+(size/2f),position.y - (size/2f),position.z),color);
				Debug.DrawLine(new Vector3(position.x-(size/2f),position.y - (size/2f),position.z),new Vector3(position.x+(size/2f),position.y + (size/2f),position.z),color);
			}
		}

		public static void DrawCross(Vector3 position, float size, Color color)
		{
			Debug.DrawLine(new Vector3(position.x-(size/2f),position.y,position.z),new Vector3(position.x+(size/2f),position.y,position.z),color);
			Debug.DrawLine(new Vector3(position.x,position.y - (size/2f),position.z),new Vector3(position.x,position.y + (size/2f),position.z),color);
		}

		public static Vector3 ReplaceComponent(this Vector3 v,VectorComponent component, float newValue )
		{
			switch(component)
			{
				case VectorComponent.X:
					return new Vector3(newValue,v.y,v.z);
				break;
				case VectorComponent.Y:
					return new Vector3(v.x,newValue,v.z);
				break;
				case VectorComponent.Z:
					return new Vector3(v.x,v.y,newValue);
				break;
			}
			return v;
		}
	}
}

public enum VectorComponent
{
	X,
	Y,
	Z
}