﻿using UnityEngine;
using System.Collections;
using MonoExtensions;

public class PlayerForceController : MonoBehaviour {

	[HideInInspector]
	public Rigidbody2D rigidbody;
	public float TurnSpeed = 500;
	public float angularVelocity = 20;
	
	public PID angleControllerPID;
	public PID angularVelocityControllerPID;
	public PID brakePID;
	public PID speedPID;

	private float torque;
	public float targetAngle;

	void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	public float turnOvershootCorrectionSpeed = 10f;
	private bool isEngineEnabled = true;
	public Vector3 mouseWorldPos;

	public float slowingDistance = 100f;
	public float stoppingDistance = 30f;

	[HideInInspector]
	public float engineForceMultiplier =0;
	private float engineRestartTime =0f;
	[HideInInspector]
	public float thrusterForceMultiplier =0f;
	private float thrustersRestartTime =0f;
	
	public float viewPortDistanceFromCenter;
	public float brakeEfficiency = 0.5f;
	public Vector2 mouseDirection;

	public float maxSpeed = 30f;
//	public float lookforwardSpeed =10f;
//	public float percentageSpeed;

	void FixedUpdate()
	{
		float dt = Time.fixedDeltaTime;
		viewPortDistanceFromCenter = Mathf.Min(Vector3.Distance(Camera.main.ScreenToViewportPoint(Input.mousePosition),new Vector3(0.5f,0.5f))/0.5f,1f);
		angularVelocity = rigidbody.angularVelocity;
		mouseWorldPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, transform.position.z - Camera.main.transform.position.z));
		mouseWorldPos = new Vector3(mouseWorldPos.x,mouseWorldPos.y,0);
		mouseDirection = (mouseWorldPos - transform.position).normalized;
		
		
		Vector3 velocityDiff = ((Vector2)rigidbody.velocity.normalized - (Vector2)transform.up);
		
  		if (isEngineEnabled) {
			float angleError = AngleDir (transform.up, mouseDirection, Vector3.forward);
			float torqueCorrectionForAngle = angleControllerPID.GetOutput (angleError, dt);
			float angularVelocityError = -rigidbody.angularVelocity;
			float torqueCorrectionForAngularVelocity = angularVelocityControllerPID.GetOutput(angularVelocityError, dt);
			torque = (torqueCorrectionForAngle + torqueCorrectionForAngularVelocity);
			DrawCircle(mouseWorldPos,slowingDistance,Color.blue);

			if(Vector2.Distance(transform.position,mouseWorldPos)>stoppingDistance)
			{
				rigidbody.AddTorque (torque * engineForceMultiplier * thrusterForceMultiplier);
			}

			float distanceFromMouse = Vector2.Distance(transform.position,mouseWorldPos);
			float k = Mathf.Min(distanceFromMouse,15f)/distanceFromMouse;


			if(Vector2.Distance(transform.position,mouseWorldPos)>slowingDistance)
			{
				float speedError = maxSpeed - rigidbody.velocity.magnitude;
				float speedCorrection = speedPID.GetOutput(speedError,dt);
				rigidbody.AddForce (-velocityDiff * turnOvershootCorrectionSpeed * thrusterForceMultiplier, ForceMode2D.Force);
				rigidbody.AddForce (transform.up * speedCorrection * k * engineForceMultiplier, ForceMode2D.Force);
			}
			else if(distanceFromMouse>stoppingDistance)
			{
				float brakeError = rigidbody.velocity.magnitude;
				float brakeCorrection = brakePID.GetOutput(brakeError,dt);
				rigidbody.AddForce (-rigidbody.velocity.normalized * brakeCorrection * brakeEfficiency, ForceMode2D.Force);
			}
			engineForceMultiplier = Mathf.Lerp(engineForceMultiplier,1f,0.8f* dt);
		}

		thrusterForceMultiplier = Mathf.Lerp(thrusterForceMultiplier,1f,0.5f* dt);

		if(Time.time>engineRestartTime)
		{
			isEngineEnabled = true;
		}
	}

	public void EngineFail(float seconds)
	{
		engineForceMultiplier =0;
		isEngineEnabled = false;
		engineRestartTime = Time.time + seconds;
	}

	public void ThrusterFail(float seconds)
	{
		thrusterForceMultiplier =0;
		thrustersRestartTime = Time.time + seconds;
	}
	
	private float AngleDir(Vector3 fwd , Vector3 targetDir ,Vector3 up ) {
		float result=0;
		Vector3 perp  = Vector3.Cross(fwd, targetDir);
		float dir  = Vector3.Dot(perp, up);
		
		if (dir > 0.0) {
			result = 1.0f;
		} else if (dir < 0.0) {
			result = -1.0f;
		}

		return result * (Vector3.Angle(fwd, targetDir));

	}

	public static void DrawCircle(Vector3 position, float radius, Color color, int points = 24)
	{
		Vector3 previousPt =  new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad * 0),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * 0),position.z);
		float step = 360f/points;
		for(float i=step;i<=360f;i+= step )
		{
			Vector3 nextPt = new Vector3(position.x + radius * Mathf.Cos (Mathf.Deg2Rad *i),position.y + radius * Mathf.Sin (Mathf.Deg2Rad * i),position.z);
			Debug.DrawLine(previousPt,nextPt,color);
			previousPt = nextPt;
		}
	}

	private void OnCollisionEnter2D(Collision2D  collisions)
	{
	
		if(collisions.relativeVelocity.magnitude>70f && collisions.transform!=null)
		{
			EngineFail(0.5f);
		}
	}
}