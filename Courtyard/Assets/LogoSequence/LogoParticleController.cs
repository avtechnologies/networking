﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;
using Holoville.HOTween;

public class LogoParticleController : MonoBehaviour {
	public Light light1;
	public Light light2;
	
	public MeshRenderer logoRender;

	public Color lColor
	{
		set{
			logoRender.material.SetColor("_TintColor",value);
		}
		get{
			return logoRender.material.GetColor("_TintColor");
		}
	}

	public Color pColor
	{
		set{
			pSystem.particleSystemRenderer.material.SetColor("_TintColor",value);
		}
		get{
			return pSystem.particleSystemRenderer.material.GetColor("_TintColor");
		}
	}

	public float pSize
	{
		set{
			pSystem.sizeMax = value;
		}
		get{
			return pSystem.sizeMax;
		}
	}
	
	private PlaygroundParticlesC pSystem;

	void Awake()
	{
		RenderSettings.ambientLight = Color.black;
		light1.intensity =0;
		light2.intensity =0;
		pSystem = GetComponent<PlaygroundParticlesC>();
		pSystem.scatterScale = Vector3.one * 20f;
		pSystem.turbulenceStrength = 31f;
	}


	// Use this for initialization
	void Start () {
		Sequence s = new Sequence(new SequenceParms().OnComplete(()=>{
			Application.LoadLevel("GameScene");
		}));
		s.Insert(2f,HOTween.To(light1,4f,new TweenParms().Prop("intensity",1.61f,false).Ease(EaseType.EaseOutQuad)));
		s.Insert(2f,HOTween.To(light2,4f,new TweenParms().Prop("intensity",1.61f,false).Ease(EaseType.EaseOutQuad)));
		
		s.Insert(0f,HOTween.To(pSystem,2f,new TweenParms().Prop("scatterScale",Vector3.zero,false).Ease(EaseType.EaseOutQuad)));
		s.Insert(2f,HOTween.To(pSystem,2f,new TweenParms().Prop("turbulenceStrength",0,false).Ease(EaseType.EaseOutQuad)));
		s.Insert(3.5f,HOTween.To (this,0.5f,new TweenParms().Prop("pColor",new Color(1f,1f,1f,0),false)));
		s.Insert(3.5f,HOTween.To (this,0.5f,new TweenParms().Prop("lColor",new Color(1f,1f,1f,1f),false)));
		s.Insert(3.0f,HOTween.To (this,0.5f,new TweenParms().Prop("pSize",0.2f,false)));

		s.Insert(7f,HOTween.To(light1,2f,new TweenParms().Prop("intensity",0f,false).Ease(EaseType.EaseOutQuad)));
		s.Insert(7f,HOTween.To(light2,2f,new TweenParms().Prop("intensity",0f,false).Ease(EaseType.EaseOutQuad)));
		s.Insert(7f,HOTween.To (this,1.5f,new TweenParms().Prop("lColor",new Color(1f,1f,1f,0f),false)));

		s.Play();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
